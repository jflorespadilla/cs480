README File for PA02
====================
Author: Jesus Flores-Padilla

Current assignment goals: Implement interactive buttons to activate and deactivate spin.

Status: complete.

Insturctions:
 r or R - Reverse the current rotation of the cube.

 Right mouse click - opens up the menu and allows you to stop and resume the rotation and to quit the program

 Left mouse click - Reverse the current rotation of the cube.

 ESC - Quit the program

Notes and other information:

So once again, the file I/O is odd and will only read in the shaders properly so long as you call the executalbe from within the bin directory.
The shaders have been included inside the shaders directory which is held in the assets directory. Assets directory is within the build directory.

Functions added were the display_demo for the menu and myMouse for the mouse processes. I made global variables rotate and reverse that help execute the events specified by the mouse and keyboard events.

One thing to note is that I did not make the rotation reversal smooth. I couldn't figure out a smooth way to reverse the spin on the cube, so it look jerky.
Another thing is that while making the cube stop is smooth, the process to resume the spin isn't smooth either. This is also something I couldn't figure out.
One last note is that most of my logical changes happend in the update function. Just in case you want to see where I was operating.
README File for PA05
====================
Author: Jesus Flores-Padilla
Partners: Jan Chris Orolfo, Sarah Koh


Current assignment goals: Make a model loader that loads in an OBJ file using ASSIMP

=====================

Version of Assimp used: 3.0

=====================
As you can see from the heading of this file I was a part of a group. Now before I explain anyfurther I would like to say that the proper working code that should be graded will be in either Sarah's or JC's repositories, not mine. There were a couple of issues that are present in mine.

Anyway, we all made a function called loadOBJ(const char* file, Vertex* &geo) which took in a file and a pointer to a Vertex struct and loaded in the model information on to the Vertex array. Basic enough. Though in some versions of our code we took into account Vertice normal in others we did not. JC and I ignored normals due to us getting segfaults anytime we tried to take into account the normals. We'd get segfaults even when reading an obj file that we knew had normals. So we stopped taking them into account and JC's program ran just fine. I don't know if mine would have ran fine as I ran into a unique issue that basically stumped me and put my code out of comission.

So my issue is that whenever my model loader function is called it somehow affects the shaders in such a way that it won't compile them. I really don't know what to make of it. As far as I know, I have identical code to that of my partners as far as the model loader goes. The only thing that differs is my shader loader. But when I examine the code I see no reason why it shouldn't work. I simply read in the file as a string and just assign the c_style string to a variable. So even though this isn't the version of code that will be graded I would love some feed back on this, because I am stumped.


------------Instructions--------------
The file path of the model will be taken in as a command line argument. So the user specifies what model to load in.

The ESC key is used to quit the program. Other controlls and such were disabled for the puropse of making coding a little bit simpler until everything was working. I never got around to re-activating the rotations and such.


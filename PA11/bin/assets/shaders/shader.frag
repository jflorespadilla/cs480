varying vec3 color;
varying vec2 texCoord;
uniform sampler2D tex;
void main(void){
gl_FragColor = texture2D(tex, texCoord) *vec4(1,1,1,1) ;
}

11/25/2013
==PA11==================================================================================
Author: Jan Chris Orolfo
Partners: Jesus Flores-Padilla and Sarah Koh

Controls:

WASD keys:  Tilt the Board mesh
IJKL Keys:  Camera Control
Right Click:  Open Menu
	1)  Reset Ball
	2)  Pause
	3)  Resume
	4)  Exit

For the Labyrinth game, we used the old air hockey for starting off.  We kept the same basic architexture, but improved on most the code. Fixing the most obvious problem from our this game, we went ahead and modified the mesh loading code to take in all the meshes.  We also implemented a working texture loader as opposed to just having a single color.  Overall, this added a huge amount of content to the previous game.  We also have a working HUD that displays the time properly.  All the controls and work properly and the basis for working physics is evident as well.  This is demonstrated by tamering with gravity to guide the ball into a hole.  However, due to the bottlenecking of many other projects and duties, we found that we did not have the time we needed to fully finish the game.  If given more time we would have addressed:
	*) Rotation of the bullet collision meshes to fit the texture and model mesh
	*) Proper Scoring of the game
	*) Lighting
	*) Reimplementation of the hard/brute force code
	-) Multiple Levels/Balls
	-) Music

Overall though, we are happy with what we have for the end product.


//MAIN DRIVER

//globals

//includes
#include "shader_loader.h"
#include "display.h"
#include "timer.h"
#include "keyboard.h"
#include "mouse.h"
#include "reshape.h"
#include "structure.h"

#include <GL/glut.h>
#include <bullet/btBulletDynamicsCommon.h>
#include <iostream>

//create object for variables
structure var;


//function prototypes
void init();
void prepareTexture(int w, int h, unsigned char* data, int w2, int h2, unsigned char* data2, int w3, int h3, unsigned char* data3);
unsigned int loadImageFromDevIL(std::string filename);

//main

int main( int argc, char **argv )
{
   int id, id2, idBall;
   unsigned char* data;
   unsigned char* data2;
   unsigned char* data3;

   //initialize glut
   var.window_w = 750;
   var.window_h = 750;

    // load image for board
    id = loadImageFromDevIL("test.jpg");
    // image not loaded
    if (id == 0)
	printf("Image could not be loaded, terminating.");

	ilBindImage(id);
	var.img1_w = ilGetInteger(IL_IMAGE_WIDTH);
	var.img1_h = ilGetInteger(IL_IMAGE_HEIGHT);
	data = ilGetData();

    // load image for board
    idBall = loadImageFromDevIL("ballTex.jpg");
    // image not loaded
    if (idBall == 0)
	printf("Image could not be loaded, terminating.");

	ilBindImage(idBall);
	var.img2_w = ilGetInteger(IL_IMAGE_WIDTH);
	var.img2_h = ilGetInteger(IL_IMAGE_HEIGHT);
	data2 = ilGetData();

    // load image for board2
    id2 = loadImageFromDevIL("test2.jpg");
    // image not loaded
    if (id2 == 0)
	printf("Image could not be loaded, terminating.");

	ilBindImage(id2);
	var.img3_w = ilGetInteger(IL_IMAGE_WIDTH);
	var.img3_h = ilGetInteger(IL_IMAGE_HEIGHT);
	data3 = ilGetData();

   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
   glutInitWindowSize(var.window_w, var.window_h);
   glutCreateWindow("Labyrinth");

    GLenum status = glewInit();
    if( status != GLEW_OK)
    {
        std::cerr << "[F] GLEW NOT INITIALIZED: ";
        std::cerr << glewGetErrorString(status) << std::endl;
        return -1;
    }

   // set up texture for data
   prepareTexture(var.img1_w, var.img1_h, data, var.img2_w, var.img2_h, data2, var.img3_w, var.img3_h, data3);

   //run init
   init();
   
   initMenu();

   //glut callback functions
   glutDisplayFunc(display);
   glutTimerFunc(17, update, 0);
   glutKeyboardFunc(keyPress);
   glutMouseFunc(mouseEvent);
   glutMotionFunc(mouseMove);
   glutReshapeFunc(reshape);

   glutMainLoop();
}


//function implementation
void init()
{
   //initalize openGL
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);


   //Load shaders 
   var.program = loadShaders(var.loc_position, var.loc_color, var.loc_tex, var.loc_mvpmat);

   //initialize board 
   var.board1 = new object("assets/models/board.obj", 0.5, var.program,var.textureID);
   var.board2 = new object("assets/models/board.obj", 0.5, var.program,var.textureID3);
   var.board3 = new object("assets/models/board.obj", 0.5, var.program,var.textureID2);

   //                       *** This may need attention ***
   object walls("assets/models/board.obj", 0.5, var.program,var.textureID);
   
   //initialize ball
   var.ball = new object("assets/models/ball.obj", 0.5, var.program, var.textureID2);

   var.board1->initBuffersBoard1();
   var.board2->initBuffersBoard2();
   var.board3->initBuffersBoard3();
   //                       *** This may need attention *** 
   walls.initBuffersBoard1();

   var.ball->initBuffersBall();

   //set attrib and uniform locations

   var.board1->setAttribLocs(var.loc_position, var.loc_color, var.loc_tex, var.loc_mvpmat);
   var.board2->setAttribLocs(var.loc_position, var.loc_color, var.loc_tex, var.loc_mvpmat);
   var.board3->setAttribLocs(var.loc_position, var.loc_color, var.loc_tex, var.loc_mvpmat);
   //                       *** This may need attention ***
   walls.setAttribLocs(var.loc_position, var.loc_color,var.loc_tex, var.loc_mvpmat);

   var.ball->setAttribLocs(var.loc_position, var.loc_color,var.loc_tex, var.loc_mvpmat);

   //set camera position and perspective
   var.zNear = 0.01;
   var.zFar = 100.0;
   var.fov = 60.0;
   var.ratio = 1.0; 

   var.projection = glm::perspective(var.fov, var.ratio, var.zNear, var.zFar);

   // set up initial camera position
   var.camPos.x = 6.0;
   var.camPos.y = 7.0;
   var.camPos.z = 8.0;
   var.look.x = 0.0;
   var.look.y = 0.0;
   var.look.z = 0.0;
   var.up.x = 0.0;
   var.up.y = 1.0;
   var.up.z = 0.0;   

   var.view = glm::lookAt( var.camPos, var.look, var.up ); 

   glEnable(GL_DEPTH_TEST);
   glDepthFunc(GL_LESS);

   //get and set size of board
   glm::vec3 boardSize = var.board1->get_dimensions();

   //get and set size of ball
   double radius = var.ball->x_width/2.0;
   double height = var.ball->y_width;
   glm::vec2 ballSize = glm::vec2( radius, height);

   //get center of objects
   glm::vec3 boardCent = var.board1->center;
   glm::vec3 ballCent = var.ball->center;

   //initialize physics

   var.phys.init( boardSize, ballSize, 
                  boardCent, ballCent,
                  walls.geometry, walls.vCount1);
}


unsigned int loadImageFromDevIL(std::string filename)
{
    ILboolean success;
unsigned int imageID;
// init DevIL. This needs to be done only once per application
ilInit();
// generate an image name
ilGenImages(1, &imageID);
// bind it
ilBindImage(imageID);
// match image origin to OpenGL’s
ilEnable(IL_ORIGIN_SET);
ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
// load  the image
success = ilLoadImage((ILstring)filename.c_str());
	ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
// check to see if everything went OK
if (!success) {
    ilDeleteImages(1, &imageID);
 
    return 0;
   }
	else return imageID;
}

void prepareTexture(int w, int h, unsigned char* data, int w2, int h2, unsigned char* data2, int w3, int h3, unsigned char* data3) 
{
    /* Create and load texture to OpenGL for board*/
    glGenTextures(1, &(var.textureID)); /* Texture name generation */
    glBindTexture(GL_TEXTURE_2D, var.textureID); 
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 
                var.img1_w, var.img1_h, 
                0, GL_RGBA, GL_UNSIGNED_BYTE,
                data); 
    glGenerateMipmap(GL_TEXTURE_2D);


    // Now loadin texture for ball 
    glGenTextures(1, &(var.textureID2)); /* Texture name generation */
    glBindTexture(GL_TEXTURE_2D, var.textureID2); 
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 
                var.img2_w, var.img2_h, 
                0, GL_RGBA, GL_UNSIGNED_BYTE,
                data2); 
    glGenerateMipmap(GL_TEXTURE_2D);

    glGenTextures(1, &(var.textureID3)); /* Texture name generation */
    glBindTexture(GL_TEXTURE_2D, var.textureID3); 
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 
                var.img3_w, var.img3_h, 
                0, GL_RGBA, GL_UNSIGNED_BYTE,
                data3); 
    glGenerateMipmap(GL_TEXTURE_2D);

}


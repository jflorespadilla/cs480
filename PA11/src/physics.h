#ifndef __PHYSICS_H
#define __PHYSICS_H
#include "object.h"
#include <vector>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <btBulletDynamicsCommon.h>

class physics
{
   public:
      physics();

      ~physics();

      void init( glm::vec3 const& boardSize, glm::vec2 const& ballSize,
                 glm::vec3 const& boardCent, glm::vec3 const& ballCent,
                 object::Vertex* geo, const int num_boardPoints);

      btCollisionShape* boardShape;
      btCollisionShape* ballShape;

      btRigidBody* ballRigidBody;
      btRigidBody* boardRigidBody;
      btDiscreteDynamicsWorld* dynamicsWorld;
     
      btGeneric6DofConstraint *ballXZplaneConstraint;

      btTransform board_trans;
      btTransform ball_trans;
      
      glm::vec3 ball_pre_motion;
      glm::vec3 ball_post_motion;
      

      btDefaultCollisionConfiguration* collisionConfiguration;
      btCollisionDispatcher* dispatcher;
      btBroadphaseInterface* overlappingPairCache;
      btSequentialImpulseConstraintSolver* solver;

      btDefaultMotionState* boardMotionState;
      btDefaultMotionState* ballMotionState;

      btScalar board_friction;
      btScalar board_restitution;
      
      btScalar ball_mass;
      btScalar ball_friction;
      btScalar ball_restitution;

      int shift_z_pos1;
      int shift_z_neg1;
      int shift_z_pos2;
      int shift_z_neg2;
      int shift_x_pos1;
      int shift_x_neg1;
      int shift_x_pos2;
      int shift_x_neg2;

      int action;
};


#endif



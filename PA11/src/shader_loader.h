#ifndef __SHADER_LOADER_H
#define __SHADER_LOADER_H
#include <GL/glew.h>
#include <GL/glut.h>
#include <iostream>
#include <fstream>

void readInput (const char* file, std::string& output);

GLuint loadShaders(GLint& locP, GLint& locC, GLint& locT, GLint& locMVP);

#endif

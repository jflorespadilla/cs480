#include "mouse.h"

void mouseEvent(int button, int state, int x, int y)
{
   if(button == GLUT_LEFT_BUTTON)
   {
      //Get the current mouse position 
      glm::vec3 mousePos;

      //retrieve the viewport data
      GLint viewport[4];
      glGetIntegerv(GL_VIEWPORT, viewport );
 
      //retrieve the modelview 
      GLdouble modelview[16];
      glGetDoublev(GL_MODELVIEW_MATRIX, modelview);

      //retrieve the projection matrix 
      GLdouble proj[16];
      glGetDoublev(GL_PROJECTION_MATRIX,proj);

      GLdouble worldX = x;
      GLdouble worldY = viewport[3]-y;
      GLdouble worldZ=0;

      //compute the world coordinates first point
      GLdouble posX, posY, posZ;
      gluUnProject( worldX, worldY, worldZ, modelview, proj, viewport, &posX, &posY, &posZ);

   }
}


void mouseMove(int x, int y)
{
      //Get the current mouse position 
      glm::vec3 mousePos;
      //retrieve the viewport data
      GLint viewport[4];
      glGetIntegerv(GL_VIEWPORT, viewport );
 
      //retrieve the modelview 
      GLdouble modelview[16];
      glGetDoublev(GL_MODELVIEW_MATRIX, modelview);

      //retrieve the projection matrix 
      GLdouble proj[16];
      glGetDoublev(GL_PROJECTION_MATRIX,proj);

      GLdouble worldX = x;
      GLdouble worldY = viewport[3]-y;
      GLdouble worldZ=0;

      //compute the world coordinates first point
      GLdouble posX, posY, posZ;
      gluUnProject( worldX, worldY, worldZ, modelview, proj, viewport, &posX, &posY, &posZ);
}

#ifndef __MOUSE_H
#define __MOUSE_H

#include "structure.h"
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

extern structure var;

void mouseEvent(int button, int state, int x, int y);

void mouseMove(int x, int y);

#endif

#ifndef __OBJECT_H
#define __OBJECT_H

#include <string>
#include <iostream>
#include <vector>
#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> //Makes passing matrices to shaders easier
#include <IL/il.h>
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "assimp/color4.h"

using namespace std;

class object
{
   public:

      object(const char* file, const double& scale, const GLuint prog, GLuint texID);

      object();

      ~object();

      struct Vertex
      {
         GLfloat position[3];
         GLfloat normal[3];
         GLfloat color[3];
         GLfloat textureCoords[2];
      };
      GLuint vbo_geometry;// VBO handle for our geometry
      Vertex* geometry;
      const char* filename;

      bool textureLoad();

      bool loadOBJBall(const char *file, Vertex* &geo );

      bool loadOBJBoard1(const char *file, Vertex* &geom );

      bool loadOBJBoard2(const char *file, Vertex* &geom );

      bool loadOBJBoard3(const char *file, Vertex* &geom );

      void initBuffersBall();

      void initBuffersBoard1();

      void initBuffersBoard2();

      void initBuffersBoard3();

      void drawBall(glm::mat4& mvpMat, glm::mat4 viewMat, glm::mat4 projMat);

      void drawBoard1(glm::mat4& mvpMat, glm::mat4 viewMat, glm::mat4 projMat);

      void drawBoard2(glm::mat4& mvpMat, glm::mat4 viewMat, glm::mat4 projMat);

      void drawBoard3(glm::mat4& mvpMat, glm::mat4 viewMat, glm::mat4 projMat);
      // attribute types
      GLuint program;
      GLuint textureID;

      //attribute locations
      GLint loc_position;
      GLint loc_color;
      GLint loc_norm;
      GLint loc_mvpmat;
      GLint loc_texCoords;
      void setAttribLocs(GLint locP, GLint locC, GLint locT, GLint locMVP); // used to get locations of attributes and uniforms from shader

      int vCount0;
      int vCount1;// number of vertices
      int vCount2; 
      int vCount3;

      glm::mat4 model;
      glm::vec3 translation;
      glm::mat4 rotation;
      glm::vec3 velocity;

      void adjust_translation( const glm::vec3& motion );
      void set_translation( const glm::vec3& trans );
      void set_rotation( const glm::mat4& rotMat );
      glm::vec3 get_dimensions()const;

      double x_width;
      double y_width;
      double z_width;
      glm::vec3 center;

      double scaleFactor;

};

#endif

#include "object.h"

object::object( const char* file, const double& sf, const GLuint prog, GLuint texID )
{
   program = prog;
   textureID = texID;
   scaleFactor = sf;
   filename = file;
   velocity = glm::vec3(0.0,0.0,0.0);
   translation.x = 0;
   translation.y = 0;
   translation.z = 0;

}

object::object()
{

}

object::~object()
{

}

//Object loader for ball
bool object::loadOBJBall(const char *file, Vertex* &geom )
{

    Assimp::Importer importer;

    //load file
    const aiScene *scene = importer.ReadFile(file, aiProcess_GenNormals |aiProcess_Triangulate );
	
    if(!scene)
    {
        printf("ERROR: scene failed to import.\n");
        return false;
    }

    //getting number of vertices
    vCount0 = (*scene->mMeshes)->mNumVertices;

    //creating geometry of correct size
    geom = new Vertex[vCount0];
	

    aiVector3D *verts = (*scene->mMeshes)->mVertices;
    aiVector3D *mNorms = (*scene->mMeshes)->mNormals;
    aiVector3D *tex = (*scene->mMeshes)->mTextureCoords[0];



    // Load Verts
    if (((*scene->mMeshes)->HasPositions()))
    {
        for (int i = 0; i < vCount0; i++)
	    {
	     geom[i].position[0] = scaleFactor*verts[i].x;
	     geom[i].position[1] = scaleFactor*verts[i].y;
	     geom[i].position[2] = scaleFactor*verts[i].z;
	    }
    }

    // Load normals
    if (((*scene->mMeshes)->HasNormals())){
        for (int i = 0; i < vCount0; i++)
    	    {
	     geom[i].normal[0] = mNorms[i].x;
	     geom[i].normal[1] = mNorms[i].y;
	     geom[i].normal[2] = mNorms[i].z;
	    }
    }

    // Load Texture Coords

    if (((*scene->mMeshes)->HasTextureCoords(0))){
        for (int i = 0; i < vCount0; i++)
    	    {
	     geom[i].textureCoords[0] = tex[i].x;
             geom[i].textureCoords[1] = 1-tex[i].y;

     	    }
    }




    // Load random colors
    for (int i = 0; i < vCount0; i++)
	{
	 geom[i].color[0] = 1.0;
	 geom[i].color[1] = 1.0;
	 geom[i].color[2] = 1.0;
	}
    return true;
}


///////////////////////////////////////////////////////////////////////////

bool object::loadOBJBoard1 (const char *file, Vertex* &geom )
{
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(file, aiProcess_Triangulate);


    if(!scene)
        {
         std::cout << importer.GetErrorString() << std::endl;
         return false;
        }

    vCount1 = scene->mMeshes[0]->mNumVertices;

    geom = new Vertex[vCount1];

    int meshVerts1 = scene->mMeshes[0]->mNumVertices;
   

    aiVector3D *verts1 = scene->mMeshes[0]->mVertices;
    aiVector3D *mNorms1 = scene->mMeshes[0]->mNormals;
    aiVector3D *tex1 = scene->mMeshes[0]->mTextureCoords[0];

    // Load Verts
    if ((scene->mMeshes[0]->HasPositions()))
    {

        for (int i = 0; i < meshVerts1; i++)
	    {
	     geom[i].position[0] = scaleFactor*verts1[i].x;
	     geom[i].position[1] = scaleFactor*verts1[i].y;
	     geom[i].position[2] = scaleFactor*verts1[i].z;
	    }
    }

    // Load normals
    if ((scene->mMeshes[0]->HasNormals()))
    {
        for (int i = 0; i < meshVerts1; i++)
	    {
	     geom[i].normal[0] = mNorms1[i].x;
	     geom[i].normal[1] = mNorms1[i].y;
	     geom[i].normal[2] = mNorms1[i].z;
	    }
    }


    // Load Texture Coords

    if ((scene->mMeshes[0]->HasTextureCoords(0)))
    {
        for (int i = 0; i < meshVerts1; i++)
	    {
	     geom[i].textureCoords[0] = tex1[i].x;
             geom[i].textureCoords[1] = 1-tex1[i].y;
     	    }
    }

    // Load random colors
    for (int i = 0; i < vCount1; i++)
	{
	 geom[i].color[0] = 1.0;
	 geom[i].color[1] = 1.0;
	 geom[i].color[2] = 1.0;
	}
    return true;
}

///////////////////////////////////////////////////////////////////////////

// Object loader for board
bool object::loadOBJBoard2 (const char *file, Vertex* &geom )
{
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(file, aiProcess_Triangulate);

    if(!scene)
        {
         std::cout << importer.GetErrorString() << std::endl;
         return false;
        }

    vCount2 = scene->mMeshes[1]->mNumVertices;	
    geom = new Vertex[vCount2];


    int meshVerts1 = scene->mMeshes[1]->mNumVertices;
    
    aiVector3D *verts2 = scene->mMeshes[1]->mVertices;
    aiVector3D *mNorms2 = scene->mMeshes[1]->mNormals;
    aiVector3D *tex2 = scene->mMeshes[1]->mTextureCoords[0];


    // Load Verts


    if ((scene->mMeshes[1]->HasPositions()))
    {
        for (int i = 0; i < vCount2; i++)
	    {
	     geom[i].position[0] = scaleFactor*verts2[i].x;
	     geom[i].position[1] = scaleFactor*verts2[i].y;
	     geom[i].position[2] = scaleFactor*verts2[i].z;
	    }
    }

    // Load normals
    if ((scene->mMeshes[1]->HasNormals()))
    {
        for (int i = 0; i < vCount2; i++)
	    {	     
	     geom[i].normal[0] = mNorms2[i].x;
	     geom[i].normal[1] = mNorms2[i].y;
	     geom[i].normal[2] = mNorms2[i].z;
	    }
    }

    // Load Texture Coords
    if ((scene->mMeshes[1]->HasTextureCoords(0)))
    {
        for (int i = 0; i < vCount2; i++)
	    {	     
	     geom[i].textureCoords[0] = tex2[i].x;
             geom[i].textureCoords[1] = 1-tex2[i].y;
     	    }
    }




    // Load random colors
    for (int i = 0; i < vCount2; i++)
	{
	 geom[i].color[0] = 1.0;
	 geom[i].color[1] = 1.0;
	 geom[i].color[2] = 1.0;
	}
    return true;
}

bool object::loadOBJBoard3 (const char *file, Vertex* &geom )
{
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(file, aiProcess_Triangulate);

    if(!scene)
        {
         std::cout << importer.GetErrorString() << std::endl;
         return false;
        }

    vCount3 = scene->mMeshes[2]->mNumVertices;	
    geom = new Vertex[vCount3];


    int meshVerts1 = scene->mMeshes[2]->mNumVertices;
    
    aiVector3D *verts2 = scene->mMeshes[2]->mVertices;
    aiVector3D *mNorms2 = scene->mMeshes[2]->mNormals;
    aiVector3D *tex2 = scene->mMeshes[2]->mTextureCoords[0];


    // Load Verts


    if ((scene->mMeshes[2]->HasPositions()))
    {
        for (int i = 0; i < vCount3; i++)
	    {
	     geom[i].position[0] = scaleFactor*verts2[i].x;
	     geom[i].position[1] = scaleFactor*verts2[i].y;
	     geom[i].position[2] = scaleFactor*verts2[i].z;
	    }
    }

    // Load normals
    if ((scene->mMeshes[2]->HasNormals()))
    {
        for (int i = 0; i < vCount3; i++)
	    {	     
	     geom[i].normal[0] = mNorms2[i].x;
	     geom[i].normal[1] = mNorms2[i].y;
	     geom[i].normal[2] = mNorms2[i].z;
	    }
    }

    // Load Texture Coords
    if ((scene->mMeshes[2]->HasTextureCoords(0)))
    {
        for (int i = 0; i < vCount3; i++)
	    {	     
	     geom[i].textureCoords[0] = tex2[i].x;
             geom[i].textureCoords[1] = 1-tex2[i].y;
     	    }
    }




    // Load random colors
    for (int i = 0; i < vCount3; i++)
	{
	 geom[i].color[0] = 1.0;
	 geom[i].color[1] = 1.0;
	 geom[i].color[2] = 1.0;
	}
    return true;
}


void object::initBuffersBall()
{
   // load model
   loadOBJBall( filename, geometry );

   //get width of object
   double x_min = geometry[0].position[0];
   double x_max = geometry[0].position[0];
   double y_min = geometry[0].position[1];
   double y_max = geometry[0].position[1];
   double z_min = geometry[0].position[2];
   double z_max = geometry[0].position[2];

   for( int i=0; i < vCount0; i++)
   {
      if( geometry[i].position[0] < x_min )
         x_min = geometry[i].position[0];
      if( geometry[i].position[0] > x_max )
         x_max = geometry[i].position[0];
      if( geometry[i].position[1] < y_min )
         y_min = geometry[i].position[1];
      if( geometry[i].position[1] > y_max )
         y_max = geometry[i].position[1];
      if( geometry[i].position[2] < z_min )
         z_min = geometry[i].position[2];
      if( geometry[i].position[2] > z_max )
         z_max = geometry[i].position[2];
   }

   x_width = x_max - x_min;
   y_width = y_max - y_min;
   z_width = z_max - z_min;
   center = glm::vec3( x_width/2.0+x_min, y_width/2.0+y_min, z_width/2.0+z_min);

   //initialize buffer object
   glGenBuffers(1, &vbo_geometry);
   glBindBuffer(GL_ARRAY_BUFFER, vbo_geometry);
   glBufferData(GL_ARRAY_BUFFER, vCount0 * sizeof(Vertex), geometry, GL_STATIC_DRAW);

}

void object::initBuffersBoard1()
{
   // load model
   loadOBJBoard1( filename, geometry );

   //get width of object
   double x_min = geometry[0].position[0];
   double x_max = geometry[0].position[0];
   double y_min = geometry[0].position[1];
   double y_max = geometry[0].position[1];
   double z_min = geometry[0].position[2];
   double z_max = geometry[0].position[2];

   for( int i=0; i < vCount1; i++)
   {
      if( geometry[i].position[0] < x_min )
         x_min = geometry[i].position[0];
      if( geometry[i].position[0] > x_max )
         x_max = geometry[i].position[0];
      if( geometry[i].position[1] < y_min )
         y_min = geometry[i].position[1];
      if( geometry[i].position[1] > y_max )
         y_max = geometry[i].position[1];
      if( geometry[i].position[2] < z_min )
         z_min = geometry[i].position[2];
      if( geometry[i].position[2] > z_max )
         z_max = geometry[i].position[2];
   }

   x_width = x_max - x_min;
   y_width = y_max - y_min;
   z_width = z_max - z_min;
   center = glm::vec3( x_width/2.0+x_min, y_width/2.0+y_min, z_width/2.0+z_min);

   //initialize buffer object
   glGenBuffers(1, &vbo_geometry);
   glBindBuffer(GL_ARRAY_BUFFER, vbo_geometry);
   glBufferData(GL_ARRAY_BUFFER, vCount1 * sizeof(Vertex), geometry, GL_STATIC_DRAW);

}


void object::initBuffersBoard2()
{
   // load model
   loadOBJBoard2( filename, geometry );

   //get width of object
   double x_min = geometry[0].position[0];
   double x_max = geometry[0].position[0];
   double y_min = geometry[0].position[1];
   double y_max = geometry[0].position[1];
   double z_min = geometry[0].position[2];
   double z_max = geometry[0].position[2];

   for( int i=0; i < vCount2; i++)
   {
      if( geometry[i].position[0] < x_min )
         x_min = geometry[i].position[0];
      if( geometry[i].position[0] > x_max )
         x_max = geometry[i].position[0];
      if( geometry[i].position[1] < y_min )
         y_min = geometry[i].position[1];
      if( geometry[i].position[1] > y_max )
         y_max = geometry[i].position[1];
      if( geometry[i].position[2] < z_min )
         z_min = geometry[i].position[2];
      if( geometry[i].position[2] > z_max )
         z_max = geometry[i].position[2];
   }

   x_width = x_max - x_min;
   y_width = y_max - y_min;
   z_width = z_max - z_min;
   center = glm::vec3( x_width/2.0+x_min, y_width/2.0+y_min, z_width/2.0+z_min);

   //initialize buffer object
   glGenBuffers(1, &vbo_geometry);
   glBindBuffer(GL_ARRAY_BUFFER, vbo_geometry);
   glBufferData(GL_ARRAY_BUFFER, vCount2 * sizeof(Vertex), geometry, GL_STATIC_DRAW);

}


void object::initBuffersBoard3()
{
   // load model
   loadOBJBoard3( filename, geometry );

   //get width of object
   double x_min = geometry[0].position[0];
   double x_max = geometry[0].position[0];
   double y_min = geometry[0].position[1];
   double y_max = geometry[0].position[1];
   double z_min = geometry[0].position[2];
   double z_max = geometry[0].position[2];

   for( int i=0; i < vCount3; i++)
   {
      if( geometry[i].position[0] < x_min )
         x_min = geometry[i].position[0];
      if( geometry[i].position[0] > x_max )
         x_max = geometry[i].position[0];
      if( geometry[i].position[1] < y_min )
         y_min = geometry[i].position[1];
      if( geometry[i].position[1] > y_max )
         y_max = geometry[i].position[1];
      if( geometry[i].position[2] < z_min )
         z_min = geometry[i].position[2];
      if( geometry[i].position[2] > z_max )
         z_max = geometry[i].position[2];
   }

   x_width = x_max - x_min;
   y_width = y_max - y_min;
   z_width = z_max - z_min;
   center = glm::vec3( x_width/2.0+x_min, y_width/2.0+y_min, z_width/2.0+z_min);

   //initialize buffer object
   glGenBuffers(1, &vbo_geometry);
   glBindBuffer(GL_ARRAY_BUFFER, vbo_geometry);
   glBufferData(GL_ARRAY_BUFFER, vCount3 * sizeof(Vertex), geometry, GL_STATIC_DRAW);

}

void object::drawBall(glm::mat4& mvpMat, glm::mat4 viewMat, glm::mat4 projMat)
{
    // premultiply mvp
    mvpMat = projMat * viewMat * model;

    //upload the matrix to the shader
    glUniformMatrix4fv(loc_mvpmat, 1, GL_FALSE, glm::value_ptr(mvpMat));

    //set up the Vertex Buffer Object so it can be drawn
    glEnableVertexAttribArray(loc_position);
    glEnableVertexAttribArray(loc_color);
    glEnableVertexAttribArray(loc_norm);
    glEnableVertexAttribArray(loc_texCoords);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_geometry);

    // Bind texture
    glBindTexture(GL_TEXTURE_2D, textureID);

    //set pointers into the vbo for each of the attributes(position and color)
    glVertexAttribPointer( loc_position,//location of attribute
                           3,//number of elements
                           GL_FLOAT,//type
                           GL_FALSE,//normalized?
                           sizeof(Vertex),//stride
                           (void*)offsetof(Vertex,position));//offset

    glVertexAttribPointer( loc_norm,
                           3,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,normal));

    glVertexAttribPointer( loc_color,
                           3,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,color));
    glVertexAttribPointer( loc_texCoords,
                           2,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,textureCoords));

    glDrawArrays(GL_TRIANGLES, 0, vCount0);//mode, starting index, count

    //clean up
    glDisableVertexAttribArray(loc_position);
    glDisableVertexAttribArray(loc_color);
    glDisableVertexAttribArray(loc_norm);
    glDisableVertexAttribArray(loc_texCoords);
}

void object::drawBoard1(glm::mat4& mvpMat, glm::mat4 viewMat, glm::mat4 projMat)
{
    // premultiply mvp
    mvpMat = projMat * viewMat * model;

    //upload the matrix to the shader
    glUniformMatrix4fv(loc_mvpmat, 1, GL_FALSE, glm::value_ptr(mvpMat));

    //set up the Vertex Buffer Object so it can be drawn
    glEnableVertexAttribArray(loc_position);
    glEnableVertexAttribArray(loc_color);
    glEnableVertexAttribArray(loc_norm);
    glEnableVertexAttribArray(loc_texCoords);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_geometry);

    // Bind texture
    glBindTexture(GL_TEXTURE_2D, textureID);

    //set pointers into the vbo for each of the attributes(position and color)
    glVertexAttribPointer( loc_position,//location of attribute
                           3,//number of elements
                           GL_FLOAT,//type
                           GL_FALSE,//normalized?
                           sizeof(Vertex),//stride
                           (void*)offsetof(Vertex,position));//offset

    glVertexAttribPointer( loc_norm,
                           3,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,normal));

    glVertexAttribPointer( loc_color,
                           3,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,color));
    glVertexAttribPointer( loc_texCoords,
                           2,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,textureCoords));

    glDrawArrays(GL_TRIANGLES, 0, vCount1);//mode, starting index, count

    //clean up
    glDisableVertexAttribArray(loc_position);
    glDisableVertexAttribArray(loc_color);
    glDisableVertexAttribArray(loc_norm);
    glDisableVertexAttribArray(loc_texCoords);
}

void object::drawBoard2(glm::mat4& mvpMat, glm::mat4 viewMat, glm::mat4 projMat)
{
    // premultiply mvp
    mvpMat = projMat * viewMat * model;

    //upload the matrix to the shader
    glUniformMatrix4fv(loc_mvpmat, 1, GL_FALSE, glm::value_ptr(mvpMat));

    //set up the Vertex Buffer Object so it can be drawn
    glEnableVertexAttribArray(loc_position);
    glEnableVertexAttribArray(loc_color);
    glEnableVertexAttribArray(loc_norm);
    glEnableVertexAttribArray(loc_texCoords);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_geometry);

    // Bind texture
    glBindTexture(GL_TEXTURE_2D, textureID);

    //set pointers into the vbo for each of the attributes(position and color)
    glVertexAttribPointer( loc_position,//location of attribute
                           3,//number of elements
                           GL_FLOAT,//type
                           GL_FALSE,//normalized?
                           sizeof(Vertex),//stride
                           (void*)offsetof(Vertex,position));//offset

    glVertexAttribPointer( loc_norm,
                           3,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,normal));

    glVertexAttribPointer( loc_color,
                           3,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,color));
    glVertexAttribPointer( loc_texCoords,
                           2,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,textureCoords));

    glDrawArrays(GL_TRIANGLES, 0, vCount2);//mode, starting index, count

    //clean up
    glDisableVertexAttribArray(loc_position);
    glDisableVertexAttribArray(loc_color);
    glDisableVertexAttribArray(loc_norm);
    glDisableVertexAttribArray(loc_texCoords);
}


void object::drawBoard3(glm::mat4& mvpMat, glm::mat4 viewMat, glm::mat4 projMat)
{
    // premultiply mvp
    mvpMat = projMat * viewMat * model;

    //upload the matrix to the shader
    glUniformMatrix4fv(loc_mvpmat, 1, GL_FALSE, glm::value_ptr(mvpMat));

    //set up the Vertex Buffer Object so it can be drawn
    glEnableVertexAttribArray(loc_position);
    glEnableVertexAttribArray(loc_color);
    glEnableVertexAttribArray(loc_norm);
    glEnableVertexAttribArray(loc_texCoords);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_geometry);

    // Bind texture
    glBindTexture(GL_TEXTURE_2D, textureID);

    //set pointers into the vbo for each of the attributes(position and color)
    glVertexAttribPointer( loc_position,//location of attribute
                           3,//number of elements
                           GL_FLOAT,//type
                           GL_FALSE,//normalized?
                           sizeof(Vertex),//stride
                           (void*)offsetof(Vertex,position));//offset

    glVertexAttribPointer( loc_norm,
                           3,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,normal));

    glVertexAttribPointer( loc_color,
                           3,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,color));
    glVertexAttribPointer( loc_texCoords,
                           2,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,textureCoords));

    glDrawArrays(GL_TRIANGLES, 0, vCount3);//mode, starting index, count

    //clean up
    glDisableVertexAttribArray(loc_position);
    glDisableVertexAttribArray(loc_color);
    glDisableVertexAttribArray(loc_norm);
    glDisableVertexAttribArray(loc_texCoords);
}




void object::setAttribLocs(GLint locP, GLint locC, GLint locT, GLint locMVP)
{
   loc_position = locP;
   loc_color = locC;
   loc_texCoords = locT;
   loc_mvpmat = locMVP;
}

void object::adjust_translation( const glm::vec3& motion )
{
   velocity = glm::vec3(motion.x,motion.y,motion.z);
   
   translation.x += motion.x;
   translation.y += motion.y;
   translation.z += motion.z;

   model = glm::translate( glm::mat4(1.0f), translation);
   glutPostRedisplay();
}

void object::set_translation( const glm::vec3& trans )
{
   velocity = glm::vec3(trans.x,trans.y,trans.z);
   
   translation.x = trans.x;
   translation.y = trans.y;
   translation.z = trans.z;

   model = glm::translate( glm::mat4(1.0f), translation);
   glutPostRedisplay();
}

void object::set_rotation( const glm::mat4& rotMat )
{
   rotation = rotMat;
}

glm::vec3 object::get_dimensions()const
{
   return glm::vec3(x_width, y_width, z_width);
}


#include "keyboard.h"

static float angle = 2.0;
int roll = 0;
int tilt = 0;
// for camera
void keyPress( unsigned char key, int x, int y )
{
   switch(key)
   {
      case 27:
      exit(0);
      break;

      case 'w':
      if(var.pauseFlag == false)
         //var.camPos.z -= 1;
	 if (roll <5){
	 var.board1->model = glm::rotate( var.board1->model, angle, glm::vec3(-1,0,0));	 
	 var.board2->model = glm::rotate( var.board2->model, angle, glm::vec3(-1,0,0));
	 var.board3->model = glm::rotate( var.board3->model, angle, glm::vec3(-1,0,0));
	 roll++;
	 }	 	 
      break;

      case 's':
      if(var.pauseFlag == false)
         //var.camPos.z += 1;
	 if (roll > -5){
	 var.board1->model = glm::rotate( var.board1->model, angle, glm::vec3(1,0,0));	 
	 var.board2->model = glm::rotate( var.board2->model, angle, glm::vec3(1,0,0));
	 var.board3->model = glm::rotate( var.board3->model, angle, glm::vec3(1,0,0));
	 roll--;
	 }
      break;

      case 'a':
      if(var.pauseFlag == false)
         //var.camPos.x -= 1;
	 if (tilt < 5){
	 var.board1->model = glm::rotate( var.board1->model, angle, glm::vec3(0,0,1));	 
	 var.board2->model = glm::rotate( var.board2->model, angle, glm::vec3(0,0,1));
	 var.board3->model = glm::rotate( var.board3->model, angle, glm::vec3(0,0,1));
	 tilt++;
	 }
      break;

      case 'd':
      if(var.pauseFlag == false)
         //var.camPos.x += 1;
	 if (tilt > -5){
	 var.board1->model = glm::rotate( var.board1->model, angle, glm::vec3(0,0,-1));	 
	 var.board2->model = glm::rotate( var.board2->model, angle, glm::vec3(0,0,-1));
	 var.board3->model = glm::rotate( var.board3->model, angle, glm::vec3(0,0,-1));
	 tilt--;
	 }
      break;
/////////////////////////////
      case 'i':
      if(var.pauseFlag == false){
         var.camPos.z -= 1;
	 }	 	 
      break;

      case 'k':
      if(var.pauseFlag == false){
         var.camPos.z += 1;
	 }
      break;

      case 'j':
      if(var.pauseFlag == false){
         var.camPos.x -= 1;
	 }
      break;

      case 'l':
      if(var.pauseFlag == false){
         var.camPos.x += 1;
	 }
      break;


/////////////////////////////
      case 'q':
      if(var.pauseFlag == false)
         var.camPos.y -= 1;
      break;

      case 'e':
      if(var.pauseFlag == false)
         var.camPos.y += 1;
      break;

      case 'r':
      if(var.pauseFlag == false)
      {
         var.camPos.x = 4.0;
         var.camPos.y = 3.0;
         var.camPos.z = 8.0;
         var.look.x = 0.0;
         var.look.y = 0.0;
         var.look.z = 0.0;
      }
      break;

   }
}



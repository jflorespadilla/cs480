#include "display.h"

void display()
{
   glClearColor( 0.0, 0.0, 0.2, 1.0 );
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

   displayScores();
 
   if(var.winnerFlag == 1 )
      displayWinner("PLAYER 1 WINS!");
 
   else if (var.winnerFlag == 2)
      displayWinner("PLAYER 2 WINS!");
 
   glUseProgram(var.program);

   var.board1->drawBoard1(var.mvp, var.view, var.projection);      
   var.board2->drawBoard2(var.mvp, var.view, var.projection);      
   var.board3->drawBoard3(var.mvp, var.view, var.projection);      

   var.ball->drawBall(var.mvp, var.view, var.projection);

   glutSwapBuffers();
}

void initMenu()
{
   glutCreateMenu(menu);
   glutAddMenuEntry("Reset Game", 1);
   glutAddMenuEntry("Pause", 2);
   glutAddMenuEntry("Resume", 3);
   glutAddMenuEntry("Exit", 4);
   glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void menu(int key)
{
   switch(key)
   {
      case 1:
      {
         glm::vec3 motion;
         //reset score
         var.p1_score = 0;
         var.winnerFlag = 0;
	 time(&var.timeStart);
         //reset ball position
         var.phys.ballRigidBody->setWorldTransform(btTransform(btQuaternion(0,0,0,0.5),btVector3(-2.4, 3, -1)));

         //get an update of the motion state
         var.phys.ballRigidBody->getMotionState()->getWorldTransform( var.phys.ball_trans );

         //load new position into structure for paddle1 model
         motion = glm::vec3( var.phys.ball_trans.getOrigin().getX(), var.phys.ball_trans.getOrigin().getY(), var.phys.ball_trans.getOrigin().getZ());

         //push new position into ball
         var.ball->set_translation( motion );

         // reset velocity
         var.phys.ballRigidBody->setLinearVelocity( btVector3(0.0,0.0,0.0));
         var.phys.ballRigidBody->setAngularVelocity( btVector3(0.0,0.0,0.0));
         break;
      }
      case 2:
      var.pauseFlag = true;
      break;

      case 3: 
      var.pauseFlag = false;
      break;

      case 4:
      exit(0);
      break;
   }
}

void displayScores()
{
    
    char* buffer = new char[30];
    time(&var.timeSpent);

    double diff = difftime(var.timeSpent,var.timeStart);
    sprintf(buffer, "Player Time: %f seconds", diff );
    // disable shaders for text
    glUseProgram(0);

    // setting color
    glColor3f(1.0f,1.0f,1.0f);

    // setting position
    glRasterPos2f(-0.95f,0.90f);
  
    // print to screen
    while (*buffer) 
    {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *buffer);
        buffer++;
	
    }

/////////////////////////////////////////////



}

void displayWinner(const char* winner)
{
    // disable shaders for text
    glUseProgram(0);

    // setting color
    glColor3f(1.0f,1.0f,1.0f);

    // setting position
    glRasterPos2f(-0.95f,0.90f);
  
    // print to screen
    while (*winner) 
    {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *winner);
       winner++;
    }

}


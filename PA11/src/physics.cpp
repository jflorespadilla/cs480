#include "physics.h"

physics::physics()
{
   shift_x_pos1 = 0;
   shift_x_neg1 = 0;
   shift_x_pos2 = 0;
   shift_x_neg2 = 0;
   shift_z_pos1 = 0;
   shift_z_neg1 = 0;
   shift_z_pos2 = 0;
   shift_z_neg2 = 0;

   action = 0;

   ball_mass = 7.0;
   ball_friction = 0.01;
   ball_restitution = 0.9;

   board_friction = 0.5;
   board_restitution = 0.9;
   boardShape = NULL;
   ballShape = NULL;

   boardRigidBody = NULL;

   ballXZplaneConstraint = NULL;
   collisionConfiguration = NULL;

   dispatcher = NULL;
   overlappingPairCache = NULL;
   solver = NULL;

   boardMotionState = NULL;
   ballMotionState = NULL;
   dynamicsWorld = NULL;

}

physics::~physics()
{
   if ( ballRigidBody ) delete ballRigidBody;
   if ( boardRigidBody ) delete boardRigidBody;

   if ( ballXZplaneConstraint ) delete ballXZplaneConstraint;

   if ( collisionConfiguration ) delete collisionConfiguration;
   if ( dispatcher ) delete dispatcher;
   if ( overlappingPairCache ) delete overlappingPairCache;
   if ( solver ) delete solver;
   
   if ( boardMotionState ) delete boardMotionState;
   if ( ballMotionState ) delete ballMotionState;
}

void physics::init( glm::vec3 const& boardSize, glm::vec2 const& ballSize,
                 glm::vec3 const& boardCent, glm::vec3 const& ballCent,
                 object::Vertex* geo, const int num_boardPoints)
{
   //create options 
   collisionConfiguration = new btDefaultCollisionConfiguration();

   //create collision checker
   dispatcher = new btCollisionDispatcher(collisionConfiguration);

   //create Broadphase collision checker
   overlappingPairCache = new btDbvtBroadphase();

   ///the default constraint solver.
   solver = new btSequentialImpulseConstraintSolver;

   ///create dynamic environment
   dynamicsWorld = new btDiscreteDynamicsWorld( dispatcher, overlappingPairCache, solver, collisionConfiguration);

   ///set value of gravity
   // dynamicsWorld->setGravity(btVector3(0,10,0));
   dynamicsWorld->setGravity(btVector3(.1,-10,-1));

   /* Board(walls) */
   btTriangleMesh *mTriMesh = new btTriangleMesh();
   glm::vec3 a0, a1, a2;

   for(int i=0; i<num_boardPoints; i+=3)
   {
      a0 = glm::vec3( geo[i+0].position[0], geo[i+0].position[1], geo[i+0].position[2] );
      a1 = glm::vec3( geo[i+1].position[0], geo[i+1].position[1], geo[i+1].position[2] );
      a2 = glm::vec3( geo[i+2].position[0], geo[i+2].position[1], geo[i+2].position[2]);

      btVector3 v0(a0.x,a0.y,a0.z);
      btVector3 v1(a1.x,a1.y,a1.z);
      btVector3 v2(a2.x,a2.y,a2.z);

      mTriMesh->addTriangle(v0,v1,v2);
   }

   btCollisionShape *boardShape = new btBvhTriangleMeshShape(mTriMesh,true);


   //build motion state (BOARD)
   boardMotionState = new btDefaultMotionState( btTransform(btQuaternion(0,0,0,1),btVector3( 0, -0.1,0)));

   btRigidBody::btRigidBodyConstructionInfo boardRigidBodyCI( 0, boardMotionState, boardShape, btVector3(0,0,0));
   boardRigidBodyCI.m_friction = board_friction; 
   boardRigidBodyCI.m_restitution = board_restitution;

   boardRigidBody = new btRigidBody( boardRigidBodyCI);
   boardRigidBody->setCollisionFlags(boardRigidBody->getCollisionFlags()|btCollisionObject::CF_KINEMATIC_OBJECT);
   boardRigidBody->setActivationState(DISABLE_DEACTIVATION);

   dynamicsWorld->addRigidBody( boardRigidBody );

   /* Puck */
   ///build ball collision model
   ballShape = new btCylinderShape(btVector3(btScalar(ballSize.x),btScalar(0.1),btScalar(ballSize.x)));

   //build motion state (PUCK)
   ballMotionState = new btDefaultMotionState( btTransform(btQuaternion(0,0,0,1), btVector3( -2.4,3,-1)));

   btVector3 ballInertia(0,0,0);
   ballShape->calculateLocalInertia( ball_mass, ballInertia);

   btRigidBody::btRigidBodyConstructionInfo ballRigidBodyCI( ball_mass, ballMotionState, ballShape, ballInertia);
   ballRigidBodyCI.m_friction = ball_friction; //this is the friction of its surfaces
   ballRigidBodyCI.m_restitution = ball_restitution; 

   ballRigidBody = new btRigidBody( ballRigidBodyCI );
   dynamicsWorld->addRigidBody( ballRigidBody );

   ballRigidBody->setActivationState(DISABLE_DEACTIVATION);
  // ballRigidBody->setLinearFactor(btVector3(1,0,1));

 /*  {
      btTransform trans1 = btTransform::getIdentity();
      trans1.setOrigin(btVector3(0.0,20,0.0));

      ballXZplaneConstraint = new btGeneric6DofConstraint( *ballRigidBody,trans1, true );

      // lock the Y axis movement
      ballXZplaneConstraint->setLinearLowerLimit( btVector3(1,0,1));
      ballXZplaneConstraint->setLinearUpperLimit( btVector3(0,0,0));

      // lock the X, Z, rotations
      ballXZplaneConstraint->setAngularLowerLimit(btVector3(0,1,0));
      ballXZplaneConstraint->setAngularUpperLimit(btVector3(0,0,0));

      dynamicsWorld->addConstraint(ballXZplaneConstraint);
   }*/

}



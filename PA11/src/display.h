#ifndef __DISPLAY_H
#define __DISPLAY_H
#include "structure.h"
#include <time.h>
#include <string>
#include <GL/glut.h>

class structure;
extern structure var;

void display();

void initMenu();

//set up menu entries
void menu(int key);

//handle scores
void displayScores();

void displayWinner(const char* winner);
 
void drawString (void*, float, float, char *);


#endif

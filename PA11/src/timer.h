#ifndef __UPDATE_H
#define __UPDATE_H
#include "physics.h"
#include "structure.h"
#include "display.h"
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> 
#include <btBulletDynamicsCommon.h>
#include <chrono>
extern structure var;

void update(int);

float getDT();


#endif

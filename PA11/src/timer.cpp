#include "timer.h"

std::chrono::time_point<std::chrono::high_resolution_clock> t1,t2;
void update(int state)
{
   if(var.pauseFlag == false)
   {
   //step simulation
   var.phys.dynamicsWorld->stepSimulation(1.0f/60.f, 10);


   // update camera
   var.view = glm::lookAt( var.camPos, var.look, var.up ); 

    //float dt = getDT();// if you have anything moving, use dt.

   glm::vec3 motion;

   //get an update of the motion state
   var.phys.ballRigidBody->getMotionState()->getWorldTransform( var.phys.ball_trans );

      {
         var.phys.ballRigidBody->setAngularVelocity(btVector3(0.0,0.0,0.0));
         
         btMatrix3x3 rotation = var.phys.ball_trans.getBasis();

         glm::mat4 rotMat=glm::mat4(rotation[0][0],rotation[0][1],rotation[0][2],0.0,
                     rotation[1][0],rotation[1][1],rotation[1][2],0.0,
                     rotation[2][0],rotation[2][1],rotation[2][2],0.0,
                     0.0, 0.0, 0.0, 1.0);

         var.ball->set_rotation( rotMat );
      }

      //load new position for ball 
      motion = glm::vec3( var.phys.ball_trans.getOrigin().getX(), var.phys.ball_trans.getOrigin().getY(), var.phys.ball_trans.getOrigin().getZ());

      //push new position into ball
      var.ball->set_translation( motion );


      // Check if the ball has scored 
      glm::vec3 ball_pos = glm::vec3( var.phys.ball_trans.getOrigin().getX(), var.phys.ball_trans.getOrigin().getY(), var.phys.ball_trans.getOrigin().getZ());

      bool p1scored = false, offBoard = false;

      // first check if scoreing is possible

      if ( ball_pos.y > 10 || ball_pos.y < -10 ) { // offBoard
         offBoard = true;
      }
      else if ( ball_pos.z < 0.374856f && ball_pos.z > -0.374856f ) { // scoring possible
         if ( ball_pos.x > 3.6 )
            p1scored = true;
      }
      else if ( ball_pos.x > 13.6 || ball_pos.x < -13.6 ) { // offBoard
         offBoard = true;
      }

      if ( p1scored || offBoard )
      {
         //set ball for player 1 
         if ( p1scored )
         {
            var.phys.ballRigidBody->setWorldTransform(btTransform(btQuaternion(1,0,0,0),btVector3(1.2, 0, 0)));
            var.p1_score++;
            
            if( var.p1_score > 3 )
            {
               var.p1_score = 0;
               var.winnerFlag = 1;
            }
         }

         //ball off board, reset
         else if ( offBoard )
         {
            var.phys.ballRigidBody->setWorldTransform(btTransform(btQuaternion(1,0,0,0),btVector3(0, 0, 0)));
         }

         //get an update of the motion state
         var.phys.ballRigidBody->getMotionState()->getWorldTransform( var.phys.ball_trans );

         //load new position for ball
         motion = glm::vec3( var.phys.ball_trans.getOrigin().getX(), var.phys.ball_trans.getOrigin().getY(), var.phys.ball_trans.getOrigin().getZ());

         //push new position
         var.ball->set_translation( motion );

      }
   

   glutPostRedisplay();
   }
   glutTimerFunc(17, update, 0);
}


//returns the time delta
float getDT()
{
    float ret;
    t2 = std::chrono::high_resolution_clock::now();
    ret = std::chrono::duration_cast< std::chrono::duration<float> >(t2-t1).count();
    t1 = std::chrono::high_resolution_clock::now();
    return ret;
}


/*STRUCTURE CLASS- holds variables and parameters for program*/
#ifndef __STRUCTURE_H
#define __STRUCTURE_H

//includes
#include "object.h"
#include "physics.h"
#include "display.h"
#include <time.h>
#include <GL/glut.h>
#include <btBulletDynamicsCommon.h>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> //Makes passing matrices to shaders easier

class object;

class structure
{

   public:

      //constructor
      structure();


      //destructor
      ~structure();

      int x_beg, y_beg, x_cur, y_cur;

      GLuint program;
      GLuint textureID;
      GLuint textureID2;
      GLuint textureID3;

      glm::vec4 translation;
      glm::mat4 view;
      glm::mat4 rotation;
      glm::mat4 projection;
      glm::mat4 mvp;

      //attribute locations
      GLint loc_position;
      GLint loc_color;
      GLint loc_norm;
      GLint loc_tex;
      GLint loc_mvpmat;

      // for camera
      glm::vec3 camPos;
      glm::vec3 look;
      glm::vec3 up;
      GLfloat fov;
      GLfloat ratio;
      GLfloat zNear;
      GLfloat zFar;

      physics phys;

      int p1_score;
      int winnerFlag;

      //objects
      object* board1;
      object* board2;
      object* board3;
      object* ball;

      int window_w;
      int window_h;

      //texture image w and h
      int img1_w;
      int img1_h;
      int img2_w;
      int img2_h;
      int img3_w;
      int img3_h;

      // keep track of time
      time_t timeStart;
      time_t timeSpent;

      float tim;

      bool pauseFlag;
};


#endif


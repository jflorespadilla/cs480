Author: Jesus Flores-Padilla
Partners: Sarah Koh  Jan Chris Orolfo
======================================

Project 8: Using Bullet Physics
Status: Halfway complete (We got desperate basically.) Late Submission.

Instructions: None. Just make and run.

What to expect/What you will see: There will be cubes falling on to another cube. And that's it.

Summary of project progress:

So this project was not nice to us. It took us longer than it should have to install bullet. However, once installed it turned out that bullet was pretty nice. It's instruction booklet was super detailed and brought us back up to speed...sort of.
Once we had bullet up and running, we messed with the tutorials a bit and got a decent grasp of the library. It's pretty intuitive and makes sense.
Now with that said, the problems that we experienced were these:
-Finding the right way to link our libraries took forever
-Some of us didn't have the proper hardware to run some of the code from most modern tutorials (OpenGL 2.1 is basically my limit. Not bad, but not great.)
-Our code structure needed restucturing and we just didn't have the time to restrucure it.

Eventually we did find tutorials that helped us out a bit and we ended up with this. Now this is just something to turn in so we can get SOME points from this project.
We're pretty sure we've guaranteed a crappy grade for this project, but at least it works and it's something to turn in.

Currently PA09 is being built and is getting the restructuring that it needs to be proper, with shaders, textures, models and all. So we hope that we can get everything done by demo day. We're keeping our fingers crossed...and coffee near us.
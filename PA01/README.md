A simple example of matrix use in OpenGL
========================================

Building This Example
---------------------

*This example requires GLM*
*On ubuntu it can be installed with this command*

>$ sudo apt-get install libglm-dev

*On a Mac you can install GLM with this command(using homebrew)*
>$ brew install glm

To build this example just 

>$ cd build
>$ make

*If you are using a Mac you will need to edit the makefile in the build directory*

The excutable will be put in bin

Additional Notes For OSX Users
------------------------------

Ensure that the latest version of the Developer Tools is installed.

*EDIT BY JESUS FLORES-PADILLA*
Date: 9/10/13
==============================
So what was changed was now changed was the directory format. Now everything is under PA01 with no directory inbetween.

The code changes remain the same though, with main.cpp being the only changed file. A shaderloader function still loads in  shaders from outside files and assigns the string values to the initialize function.

These changes were done prior to the due date of the initial assignment and was done for no other reason than to make the directories look nice.
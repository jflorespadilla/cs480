README File for PA02
====================
Author: Jesus Flores-Padilla

Current assignment goals: Create a moon for the spinning cube.

Status: complete.

Notable changes:
The rotation reversal and pause/resume mechanics are now smoother and don't do that jerky motion as done in the last project. It's smooth now.

Things to know:
Once again, the executable must be called in bin in order for it to find the proper shader files.

Insturctions:
r or R - Reverses the spin of the cubes. This is a toggle button and will spin the cubes both directions.
Left arrow - Spins the cubes counter-clockwise. Not a toggel button, will only spin the cubes that way.
Right arrow - Spins the cubes clockwise. Not a toggel button, will only spin the cubes that way.
Right mouse click - Brings up a drop menu that has the options to quit, stop roation, and resume rotation.
Left mouse click - Reverses the spin of the cubes. This is a toggle buttion and will spin the cubes both directions.
ESC - Quits the program

Decisions that should be noted:
So as you may tell by running the program, the moon cube orbits the planet cube in a clockwise orbit. I chose this to show that I indeed made the moon orbit the planet and not cleverly made the orbit seem like an orbit around the planet rather than an orbit about the origin.

Code and other things to note:
When writing the code for this project I kind of got lost initially. I did really understand the draw function all that well. And it lead me to belive that I would not get this project down. However, through some luck and messing around, I found a way to draw the second cube. I still have no Earthly idea how I got about a solution to this problem. Anyway, you can see what I did in the render() function. Though after looking at some example code online, I can say that my solution is... messy and will need to be changed.
After that intitial problem was solved, everything else became pretty straight forward to do. I used the glm transforamtion functions and adjusted the code in update(). For the implementation of the use of arrow keys, I made another call back funtion that takes care of that special keyboard input.
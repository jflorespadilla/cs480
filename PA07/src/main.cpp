#include <GL/glew.h> // glew must be included before the main gl libs
#include <GL/glut.h> // doing otherwise causes compiler shouting
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <chrono>
#include <map>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> //Makes passing matrices to shaders easier


#include <assimp/color4.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <assimp/Importer.hpp>

#include <IL/il.h>

//--Data types
//This object will define the attributes of a vertex(position, color, etc...)
struct Vertex
{
    GLfloat position[3];
    GLfloat color[3];
    GLfloat normal[3];
    GLfloat textureCoords[3];
};

//--Evil Global variables
//Just for this example!

const char* file;
const char* image;
// States
int rotateState; // State for rotation
int translateState;
int numVerts;

bool hasNorms;
bool hasVects;
bool hasColor;
bool hasText;


std::map<std::string, GLuint> textureIdMap;

float rotateDeg; // Saved Rotaton Deg

int w = 640, h = 480;// Window size
GLuint program;// The GLSL program handle
GLuint vbo_geometry;// VBO handle for our geometry
GLuint textureID;

//uniform locations
GLint loc_mvpmat;// Location of the modelviewprojection matrix in the shader

//attribute locations
GLint loc_position;
GLint loc_color;
GLint loc_norm;
GLint loc_texCoords;

//transform matrices
glm::mat4 model;//obj->world each object should have its own model matrix
glm::mat4 view;//world->eye
glm::mat4 projection;//eye->clip
glm::mat4 mvp;//premultiplied modelviewprojection

glm::mat4 model2;
glm::mat4 mvp2;

bool assimpLoad (const char *file, Vertex* &geom);
//bool LoadGLTextures(const char *file);

//--GLUT Callbacks
void render();
void update();
void reshape(int n_w, int n_h);
void keyboard(unsigned char key, int x_pos, int y_pos);
void specialKeyboard(int key, int x_pos, int y_pos);
void mouse (int key, int state, int x, int y);
void menu (int key);

//--Resource management
bool initialize();
void cleanUp();

//--Random time things
float getDT();
std::chrono::time_point<std::chrono::high_resolution_clock> t1,t2;

unsigned int loadImageFromDevIL(std::string filename);
void prepareTexture(int w, int h, unsigned char* data);

//--Main

int main(int argc, char **argv)
{
   // initialize variables
   int id;
   unsigned char* data;

   if (argc != 2)
	{
	 file = "NONE";
	}

    else
	{
	 file = argv[1];
	}

    // load image 
	// load image first so that window opens with image size
	id = loadImageFromDevIL("test.jpg");
	// image not loaded
	if (id == 0)
		printf("Image could not be loaded, terminating.");

	ilBindImage(id);
	w = ilGetInteger(IL_IMAGE_WIDTH);
	h = ilGetInteger(IL_IMAGE_HEIGHT);
	data = ilGetData();
        printf("Finished loading image");

    // Initialize glut
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(w, h);
    // Name and create the Window
    glutCreateWindow("Assimp Texture Loading");

    // Now that the window is created the GL context is fully set up
    // Because of that we can now initialize GLEW to prepare work with shaders
    GLenum status = glewInit();
    if( status != GLEW_OK)
    {
        std::cerr << "[F] GLEW NOT INITIALIZED: ";
        std::cerr << glewGetErrorString(status) << std::endl;
        return -1;
    }

    // Set all of the callbacks to GLUT that we need
    glutDisplayFunc(render);// Called when its time to display
    glutReshapeFunc(reshape);// Called if the window is resized
    glutIdleFunc(update);// Called if there is nothing else to do
    glutKeyboardFunc(keyboard);// Called if there is keyboard input
    glutSpecialFunc(specialKeyboard);
    glutMouseFunc(mouse); // Called if there is mouse input

    // Menu Set-Up
    glutCreateMenu(menu);
    glutAddMenuEntry("start rotation", 1);
    glutAddMenuEntry("stop rotation", 2);
    glutAddMenuEntry("quit", 3);
    glutAttachMenu(GLUT_RIGHT_BUTTON);


    // set up texture for data
    prepareTexture(w,h,data);

    // Initialize all of our resources(shaders, geometry)
    bool init = initialize();
    if(init)
    {
        t1 = std::chrono::high_resolution_clock::now();
        glutMainLoop();
    }

    // Clean up after ourselves
    cleanUp();
    return 0;
}

//--Implementations
void render()
{
    //--Render the scene

    //clear the screen
    glClearColor(0.0, 0.0, 0.2, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //premultiply the matrix for this example
    mvp = projection * view * model;
    mvp2 = projection * view * model2;

    //enable the shader program
    glUseProgram(program);

    //upload the matrix to the shader
    glUniformMatrix4fv(loc_mvpmat, 1, GL_FALSE, glm::value_ptr(mvp));

    //set up the Vertex Buffer Object so it can be drawn
    glEnableVertexAttribArray(loc_position);
    glEnableVertexAttribArray(loc_color);
    glEnableVertexAttribArray(loc_norm);
    glEnableVertexAttribArray(loc_texCoords);


    glBindBuffer(GL_ARRAY_BUFFER, vbo_geometry);

    // Bind texture
    glBindTexture(GL_TEXTURE_2D, textureID);

    //set pointers into the vbo for each of the attributes(position and color)
    glVertexAttribPointer( loc_position,//location of attribute
                           3,//number of elements
                           GL_FLOAT,//type
                           GL_FALSE,//normalized?
                           sizeof(Vertex),//stride
                           0);//offset

    glVertexAttribPointer( loc_norm,
                           3,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,normal));


    glVertexAttribPointer( loc_color,
                           3,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,color));

    glVertexAttribPointer( loc_texCoords,
                           3,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,textureCoords));

    glDrawArrays(GL_TRIANGLES, 0, numVerts);//mode, starting index, count


    glUniformMatrix4fv(loc_mvpmat, 1, GL_FALSE, glm::value_ptr(mvp2));


    //set up the Vertex Buffer Object so it can be drawn
    glEnableVertexAttribArray(loc_position);
    glEnableVertexAttribArray(loc_color);
    glEnableVertexAttribArray(loc_norm);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_geometry);
	// Bind texture
	glBindTexture(GL_TEXTURE_2D, textureID);
    //set pointers into the vbo for each of the attributes(position and color)
    glVertexAttribPointer( loc_position,//location of attribute
                           3,//number of elements
                           GL_FLOAT,//type
                           GL_FALSE,//normalized?
                           sizeof(Vertex),//stride
                           0);//offset

    glVertexAttribPointer( loc_color,
                           3,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,color));

    glVertexAttribPointer( loc_norm,
                           3,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,normal));

    glDrawArrays(GL_TRIANGLES, 0, numVerts);//mode, starting index, count
    



    //clean up
    glDisableVertexAttribArray(loc_position);
    glDisableVertexAttribArray(loc_color);
    glDisableVertexAttribArray(loc_norm);
    //swap the buffers
    glutSwapBuffers();
}

void update()
{
    
    //total time
    static float angle = rotateDeg;
    float dt = getDT();// if you have anything moving, use dt.
    if (translateState == 1)
	{
         angle += dt * M_PI/2; //move through 90 degrees a second
	}

    else 
	{
	 angle -= dt * M_PI/2;
	}
    
    model = glm::translate( glm::mat4(1.0f), glm::vec3(4.0 * sin(angle), 0.0, 4.0 * cos(angle)));

    model2 = glm::translate( model, glm::vec3(4.0 * sin(angle), 0.0, 4.0 * cos(angle)));
    model2 = glm::scale( model2, glm::vec3(.5,.5,.5));

    if (rotateState == 1)
	{
	model = glm::rotate( model, -90*angle, glm::vec3(0,1,0));
	model2 = glm::rotate( model2, 90*angle, glm::vec3(0,1,0));
	}

    if (rotateState == -1)
	{
	model = glm::rotate( model, 90*angle, glm::vec3(0,1,0));
	model2 = glm::rotate( model2, -90*angle, glm::vec3(0,1,0));

	}

    // Update the state of the scene
    
    glutPostRedisplay();//call the display callback
}


void reshape(int n_w, int n_h)
{
    w = n_w;
    h = n_h;
    //Change the viewport to be correct
    glViewport( 0, 0, w, h);
    //Update the projection matrix as well
    //See the init function for an explaination
    projection = glm::perspective(45.0f, float(w)/float(h), 0.01f, 100.0f);
}

void keyboard(unsigned char key, int x_pos, int y_pos)
{
    if(key == 'r')//Rotate
    {
        rotateState = rotateState * -1;	
	rotateDeg = getDT();
    }

    if(key == GLUT_KEY_LEFT)
    {
	translateState = 1;
    }

    if(key == GLUT_KEY_RIGHT)
    {
	translateState = -1;
    }
}


void specialKeyboard(int key, int x_pos, int y_pos)
{
    if(key == GLUT_KEY_LEFT)
    {
	translateState = 1;
    }

    if(key == GLUT_KEY_RIGHT)
    {
	translateState = -1;
    }
}

void mouse (int key, int state, int x, int y)
{
    if(key ==  GLUT_LEFT_BUTTON && state == GLUT_DOWN)//Rotate
    {
        rotateState = rotateState * -1;	
	rotateDeg = getDT();
    }
}

void menu(int key)
{
    switch(key)
    {
        case 1:
	    rotateState = 1;
	    break;

        case 2:
	    rotateState = 0;
	    break;

        case 3:
     	    exit(0);
	    break;
    } 
}

unsigned int loadImageFromDevIL(std::string filename)
{
    ILboolean success;
unsigned int imageID;
// init DevIL. This needs to be done only once per application
ilInit();
// generate an image name
ilGenImages(1, &imageID);
// bind it
ilBindImage(imageID);
// match image origin to OpenGL’s
ilEnable(IL_ORIGIN_SET);
ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
// load  the image
success = ilLoadImage((ILstring)filename.c_str());
	ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
// check to see if everything went OK
if (!success) {
    ilDeleteImages(1, &imageID);
 
    return 0;
   }
	else return imageID;
}

void prepareTexture(int w, int h, unsigned char* data) {

	/* Create and load texture to OpenGL */
	glGenTextures(1, &textureID); /* Texture name generation */
	glBindTexture(GL_TEXTURE_2D, textureID); 
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 
                w, h, 
                0, GL_RGBA, GL_UNSIGNED_BYTE,
                data); 
	glGenerateMipmap(GL_TEXTURE_2D);
}

void readInput (const char* file, std::string& output)
{
    std::string buffer;
    std::ifstream fin;
    fin.open(file, std::ios::in);
    std::string stream = "";

    while(fin.good())
    {
        std::getline(fin, stream);
        buffer.append(stream); 
        buffer.append(1, '\n');
    }

    buffer.append(1, '\0');
    fin.close();
    output = buffer;
}

bool loadShaders()
{
    GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

    //Shader Sources
    std::string vsLoad;
    readInput("assets/shaders/default.vert", vsLoad);
    const char *vs = vsLoad.c_str();
    std::string fsLoad;
    readInput("assets/shaders/default.frag", fsLoad);
    const char *fs =fsLoad.c_str();


    //compile the shaders
    GLint shader_status;

    // Vertex shader first
    glShaderSource(vertex_shader, 1, &vs, NULL);
    glCompileShader(vertex_shader);
    //check the compile status
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &shader_status);
    if(!shader_status)
    {
        std::cerr << "[F] FAILED TO COMPILE VERTEX SHADER!" << std::endl;
        return false;
    }

    // Now the Fragment shader
    glShaderSource(fragment_shader, 1, &fs, NULL);
    glCompileShader(fragment_shader);
    //check the compile status
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &shader_status);
    if(!shader_status)
    {
        std::cerr << "[F] FAILED TO COMPILE FRAGMENT SHADER!" << std::endl;
        return false;
    }


    //Now we link the 2 shader objects into a program
    //This program is what is run on the GPU
    program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);
    //check if everything linked ok
    glGetProgramiv(program, GL_LINK_STATUS, &shader_status);
    if(!shader_status)
    {
        std::cerr << "[F] THE SHADER PROGRAM FAILED TO LINK" << std::endl;
        return false;
    }


    //Now we set the locations of the attributes and uniforms
    //this allows us to access them easily while rendering
    loc_texCoords = glGetUniformLocation(program, "texUnit");

    loc_position = glGetAttribLocation(program,
                    const_cast<const char*>("v_position"));
    if(loc_position == -1)
    {
        std::cerr << "[F] POSITION NOT FOUND" << std::endl;
        return false;
    }

    loc_color = glGetAttribLocation(program,
                    const_cast<const char*>("v_color"));
    if(loc_color == -1)
    {
        std::cerr << "[F] V_COLOR NOT FOUND" << std::endl;
        return false;
    }

    loc_mvpmat = glGetUniformLocation(program,
                    const_cast<const char*>("mvpMatrix"));
    if(loc_mvpmat == -1)
    {
        std::cerr << "[F] MVPMATRIX NOT FOUND" << std::endl;
        return false;
    }
    return true;
}

bool initialize()
{
    // Initialize devil
    ilInit();
    // Load shaders
    loadShaders();

    // set geometry
    Vertex* geometry; 
    assimpLoad(file, geometry);

    glGenBuffers(1, &vbo_geometry);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_geometry);
    glBufferData(GL_ARRAY_BUFFER, numVerts * sizeof(Vertex), geometry, GL_STATIC_DRAW);

    //--Geometry done
    
    //--Init the view and projection matrices
    //  if you will be having a moving camera the view matrix will need to more dynamic
    //  ...Like you should update it before you render more dynamic 
    //  for this project having them static will be fine
    view = glm::lookAt( glm::vec3(0.0, 8.0, -16.0), //Eye Position
                        glm::vec3(0.0, 0.0, 0.0), //Focus point
                        glm::vec3(0.0, 1.0, 0.0)); //Positive Y is up

    projection = glm::perspective( 45.0f, //the FoV typically 90 degrees is good which is what this is set to
                                   float(w)/float(h), //Aspect Ratio, so Circles stay Circular
                                   0.01f, //Distance to the near plane, normally a small value like this
                                   100.0f); //Distance to the far plane, 

    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    //and its done

    rotateState = 0;
    translateState = 1;
    return true;
}

void cleanUp()
{
    // Clean up, Clean up
    glDeleteProgram(program);
    glDeleteBuffers(1, &vbo_geometry);
}

//returns the time delta
float getDT()
{
    float ret;
    t2 = std::chrono::high_resolution_clock::now();
    ret = std::chrono::duration_cast< std::chrono::duration<float> >(t2-t1).count();
    t1 = std::chrono::high_resolution_clock::now();
    return ret;
}

bool assimpLoad (const char *file, Vertex* &geom )
{
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(file, aiProcess_Triangulate);


    if(!scene)
        {
         std::cout << importer.GetErrorString() << std::endl;
         return false;
        }

    numVerts = (*scene->mMeshes)->mNumVertices;	
    geom = new Vertex[numVerts];

    std::cout << numVerts << std::endl;


    aiVector3D *verts = (*scene->mMeshes)->mVertices;
    aiVector3D *mNorms = (*scene->mMeshes)->mNormals;
    aiVector3D *texture = (*scene->mMeshes)->mTextureCoords[0];

    // Load Verts
    if (((*scene->mMeshes)->HasPositions())){
//	std::cout << "FAGGOT HUNT CUMMENSE VERTS" << std::endl;

        for (int i = 0; i < numVerts; i++)
	    {
	     geom[i].position[0] = verts[i].x;
	     geom[i].position[1] = verts[i].y;
	     geom[i].position[2] = verts[i].z;



	     if (i < 5) {
	     std::cout << i << " Vertices being printed" << std::endl;
	     std::cout << verts[i].x << " " << verts[i].y << " " << verts[i].z << std::endl;

	     }
	    }
    }

    // Load normals
    if (((*scene->mMeshes)->HasNormals())){
//	std::cout << "FAGGOT HUNT CUMMENSE NORMS" << std::endl;
        for (int i = 0; i < numVerts; i++)
    	    {
	     geom[i].normal[0] = mNorms[i].x;
	     geom[i].normal[1] = mNorms[i].y;
	     geom[i].normal[2] = mNorms[i].z;
	    }
    }

    // Load Texture Coords
    if (((*scene->mMeshes)->HasTextureCoords(0))){
	std::cout << "Texture Coordinates being stored." << std::endl;
        for (int i = 0; i < numVerts; i++)
    	    {
	     geom[i].textureCoords[0] = texture[i].x;
             geom[i].textureCoords[1] = texture[i].y;
	     geom[i].textureCoords[2] = texture[i].z;


     	    }
    }


    // Load random colors
    for (int i = 0; i < numVerts; i++)
	{
	 geom[i].color[0] = 1.0;
	 geom[i].color[1] = 1.0;
	 geom[i].color[2] = 1.0;
	}
    std::cout << numVerts << std::endl;
    return true;
}



README File for PA07
====================
Author: Jesus Flores-Padilla
Partners: Jan Chris Orolfo, Sarah Koh
Repository to be Graded: Mine or Jan Chris'. Your pick. We should have the same code.

Current status of project: Complete (Kind of)
Image Loading Library used: devIL

Instructions
====================
Mouse Right Click - Drops a menu down that affects the rotation of the model and such, and gives you an option to quit the program.

Instructions Part II
====================
The executable file is named Yum and the program takes a command line argument for the model file that you want to load in. So name the path of the file as you call the executable file.

File and Directory Layout
=========================
The standard stuff is there such as bin, build, and src. In side of bin is the executeable file and a directory called assets. In assets we hold another two directories called shaders and models which hold the models and default shaders. Also in bin is test.jpg. We're not commiting that file (assuming I use git.ignore right) but a an image file of that name is expected to be there as we hardcoded the path for a texture.

The Process: Struggles, issues, and other stuff that you may or may not care about
=================================================================================
As stated in the last commit, we managed our time very poorly and became victems of the time crunch that happens when midterms roll around. Most of the time we spent was trying to install all neccessary libraries and stuff and making sure that we didn't get incompatible version of anything. That was a pain. Originally we were going to use Image Magick as our texture loader, but it turns out that Jan Chris and I did not have the proper hardware to run that library. So after much time spent installing Image Magick and what not, it turned out to be a wasted effort as we couldn't even use that library. So then we had to do the process of installing things all over again for devIL. That sucked. So after many hours of just waiting for our software to install, we finally got to coding. So the first thing we all did was try to adjust our model loader to take into account texture coodinates and such. This was a real drag because on my machine, assimp would not load in texture coordinates. It would detect whether or not the model had texture coodinates, but in never actually loaded them in. I could not figure out what was wrong. For whatever reason, the exact same code worked without a hitch for my partners. I kept getting a segfault because of that in ability to load texture coordinates. The rest of the code is our rough attempt at putting a texture around our models. Now, this code does not run on my machine, but it ran on my two partners respective machines. So I am going to assume that the code works and that my hardware just sucks. So yeah, we're keeping our fingers crossed that our code works in some way for you.
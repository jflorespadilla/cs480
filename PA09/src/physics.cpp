#include "physics.h"

physics::physics()
{
   shift_x_pos1 = 0;
   shift_x_neg1 = 0;
   shift_x_pos2 = 0;
   shift_x_neg2 = 0;
   shift_z_pos1 = 0;
   shift_z_neg1 = 0;
   shift_z_pos2 = 0;
   shift_z_neg2 = 0;

   action = 0;

   puck_mass = 7.0;
   puck_friction = 0.01;
   puck_restitution = 0.9;

   pdl_mass = 15.0;
   pdl_friction = 0.01;
   pdl_restitution = 0.9;

   board_friction = 0.5;
   board_restitution = 0.9;
   boardShape = NULL;
   puckShape = NULL;
   paddle1Shape = NULL;
   paddle2Shape = NULL;
   paddle1 = NULL;
   paddle2 = NULL;
   boardRigidBody = NULL;
   paddle1RigidBody = NULL;
   paddle2RigidBody = NULL;
   puckXZplaneConstraint = NULL;
   pdl1XZplaneConstraint = NULL;
   pdl2XZplaneConstraint = NULL;
   collisionConfiguration = NULL;
   dispatcher = NULL;
   overlappingPairCache = NULL;
   solver = NULL;
   boardMotionState = NULL;
   puckMotionState = NULL;
   paddle1MotionState = NULL;
   paddle2MotionState = NULL;
   dynamicsWorld = NULL;

}

physics::~physics()
{
   if ( boardShape ) delete boardShape;
   if ( puckShape ) delete puckShape;
   if ( paddle1 ) delete paddle1;
   if ( paddle2 ) delete paddle2;

   if ( puckRigidBody ) delete puckRigidBody;
   if ( boardRigidBody ) delete boardRigidBody;
   if ( paddle1RigidBody ) delete paddle1RigidBody;
   if ( paddle2RigidBody ) delete paddle2RigidBody;

   if ( puckXZplaneConstraint ) delete puckXZplaneConstraint;
   if ( pdl1XZplaneConstraint ) delete pdl1XZplaneConstraint;
   if ( pdl2XZplaneConstraint ) delete pdl2XZplaneConstraint;

   if ( collisionConfiguration ) delete collisionConfiguration;
   if ( dispatcher ) delete dispatcher;
   if ( overlappingPairCache ) delete overlappingPairCache;
   if ( solver ) delete solver;
   
   if ( boardMotionState ) delete boardMotionState;
   if ( puckMotionState ) delete puckMotionState;
   if ( paddle1MotionState ) delete paddle1MotionState;
   if ( paddle2MotionState ) delete paddle2MotionState;
}

void physics::init( glm::vec3 const& boardSize, glm::vec2 const& puckSize, glm::vec2 const& paddle1Size, glm::vec2 const& paddle2Size,
                 glm::vec3 const& boardCent, glm::vec3 const& puckCent, glm::vec3 const& paddle1Cent, glm::vec3 const& paddle2Cent,
                 object::Vertex* geo, const int num_boardPoints)
{
   //create options 
   collisionConfiguration = new btDefaultCollisionConfiguration();

   //create collision checker
   dispatcher = new btCollisionDispatcher(collisionConfiguration);

   //create Broadphase collision checker
   overlappingPairCache = new btDbvtBroadphase();

   ///the default constraint solver.
   solver = new btSequentialImpulseConstraintSolver;

   ///create dynamic environment
   dynamicsWorld = new btDiscreteDynamicsWorld( dispatcher, overlappingPairCache, solver, collisionConfiguration);

   ///set value of gravity
   dynamicsWorld->setGravity(btVector3(0,-10,0));

   /* Board(walls) */
   btTriangleMesh *mTriMesh = new btTriangleMesh();

   glm::vec3 a0, a1, a2;
   for(int i=0; i<num_boardPoints; i+=3)
   {
      a0 = glm::vec3( geo[i+0].position[0], geo[i+0].position[1], geo[i+0].position[2] );
      a1 = glm::vec3( geo[i+1].position[0], geo[i+1].position[1], geo[i+1].position[2] );
      a2 = glm::vec3( geo[i+2].position[0], geo[i+2].position[1], geo[i+2].position[2]);

      btVector3 v0(a0.x,a0.y,a0.z);
      btVector3 v1(a1.x,a1.y,a1.z);
      btVector3 v2(a2.x,a2.y,a2.z);

      mTriMesh->addTriangle(v0,v1,v2);
   }

   btCollisionShape *boardShape = new btBvhTriangleMeshShape(mTriMesh,true);


   //build motion state (BOARD)
   boardMotionState = new btDefaultMotionState( btTransform(btQuaternion(0,0,0,1),btVector3( 0, -0.1,0)));

   btRigidBody::btRigidBodyConstructionInfo boardRigidBodyCI( 0, boardMotionState, boardShape, btVector3(0,0,0));
   boardRigidBodyCI.m_friction = board_friction; 
   boardRigidBodyCI.m_restitution = board_restitution;

   boardRigidBody = new btRigidBody( boardRigidBodyCI);

   dynamicsWorld->addRigidBody( boardRigidBody );

   /* Puck */
   ///build puck collision model
   puckShape = new btCylinderShape(btVector3(btScalar(puckSize.x),btScalar(0.1),btScalar(puckSize.x)));

   //build motion state (PUCK)
   puckMotionState = new btDefaultMotionState( btTransform(btQuaternion(0,0,0,1), btVector3( 0,0.0,0)));

   btVector3 puckInertia(0,0,0);
   puckShape->calculateLocalInertia( puck_mass, puckInertia);

   btRigidBody::btRigidBodyConstructionInfo puckRigidBodyCI( puck_mass, puckMotionState, puckShape, puckInertia);
   puckRigidBodyCI.m_friction = puck_friction; //this is the friction of its surfaces
   puckRigidBodyCI.m_restitution = puck_restitution; //this is the "bounciness"

   puckRigidBody = new btRigidBody( puckRigidBodyCI );
   dynamicsWorld->addRigidBody( puckRigidBody );

   puckRigidBody->setActivationState(DISABLE_DEACTIVATION);
   puckRigidBody->setLinearFactor(btVector3(1,0,1));

   {
      btTransform trans1 = btTransform::getIdentity();
      trans1.setOrigin(btVector3(0.0,20,0.0));

      puckXZplaneConstraint = new btGeneric6DofConstraint( *puckRigidBody,trans1, true );

      // lock the Y axis movement
      puckXZplaneConstraint->setLinearLowerLimit( btVector3(1,0,1));
      puckXZplaneConstraint->setLinearUpperLimit( btVector3(0,0,0));

      // lock the X, Z, rotations
      puckXZplaneConstraint->setAngularLowerLimit(btVector3(0,1,0));
      puckXZplaneConstraint->setAngularUpperLimit(btVector3(0,0,0));

      dynamicsWorld->addConstraint(puckXZplaneConstraint);
   }

   /* Paddle1*/ 
   ///build paddle1 collision model
   paddle1 = new btCylinderShape(btVector3(btScalar(paddle1Size.x),btScalar(0.1),btScalar(paddle1Size.x)));
   paddle1Shape = paddle1;

   //build motion state (PADDLE1)
   paddle1MotionState = new btDefaultMotionState( btTransform(btQuaternion(0,0,0,1), btVector3( paddle1Cent.x,0.0, paddle1Cent.z)));

   btVector3 paddle1Inertia(0,0,0);
   paddle1Shape->calculateLocalInertia( pdl_mass, paddle1Inertia);

   btRigidBody::btRigidBodyConstructionInfo paddle1RigidBodyCI( pdl_mass, paddle1MotionState, paddle1Shape, paddle1Inertia);
   paddle1RigidBody = new btRigidBody( paddle1RigidBodyCI );
   paddle1RigidBodyCI.m_friction = pdl_friction; 
   paddle1RigidBodyCI.m_restitution = pdl_restitution; 
   
   dynamicsWorld->addRigidBody( paddle1RigidBody );

   paddle1RigidBody->setActivationState(DISABLE_DEACTIVATION);
   paddle1RigidBody->setLinearFactor(btVector3(1,0,1));
   
   {
      btTransform trans2 = btTransform::getIdentity();
      trans2 .setOrigin(btVector3(0.0,0.0,0.0));

      pdl1XZplaneConstraint = new btGeneric6DofConstraint( *paddle1RigidBody,trans2, true );

      // lock the Y axis movement
      pdl1XZplaneConstraint->setLinearLowerLimit( btVector3(1,0,1));
      pdl1XZplaneConstraint->setLinearUpperLimit( btVector3(0,0,0));

      // lock the X, Z, rotations
      pdl1XZplaneConstraint->setAngularLowerLimit(btVector3(0,1,0));
      pdl1XZplaneConstraint->setAngularUpperLimit(btVector3(0,0,0));

      dynamicsWorld->addConstraint(pdl1XZplaneConstraint);
   }

   /* Paddle2 */
   ///build paddle2 collision model
   paddle2 = new btCylinderShape(btVector3(btScalar(paddle2Size.x),btScalar(0.1),btScalar(paddle2Size.x)));

   paddle2Shape = paddle2;

   //build motion state (PADDLE2)
   paddle2MotionState = new btDefaultMotionState( btTransform(btQuaternion(0,0,0,1), btVector3( paddle2Cent.x,0.0, paddle2Cent.z)));

   btVector3 paddle2Inertia(0,0,0);
   paddle2Shape->calculateLocalInertia( pdl_mass, paddle2Inertia);

   btRigidBody::btRigidBodyConstructionInfo paddle2RigidBodyCI( pdl_mass, paddle2MotionState, paddle2Shape, paddle2Inertia);
   paddle2RigidBody = new btRigidBody( paddle2RigidBodyCI );
   paddle2RigidBodyCI.m_friction = pdl_friction; 
   paddle2RigidBodyCI.m_restitution = pdl_restitution; 
   
   dynamicsWorld->addRigidBody( paddle2RigidBody );

   paddle2RigidBody->setActivationState(DISABLE_DEACTIVATION);
   paddle2RigidBody->setLinearFactor(btVector3(1,0,1));
   
   {
      btTransform frameInB = btTransform::getIdentity();
      frameInB.setOrigin(btVector3(0.0,0.0,0.0));

      pdl2XZplaneConstraint = new btGeneric6DofConstraint( *paddle2RigidBody,frameInB, true );

      // lock the Y axis movement
      pdl2XZplaneConstraint->setLinearLowerLimit( btVector3(1,0,1));
      pdl2XZplaneConstraint->setLinearUpperLimit( btVector3(0,0,0));

      // lock the X, Z, rotations
      pdl2XZplaneConstraint->setAngularLowerLimit(btVector3(0,1,0));
      pdl2XZplaneConstraint->setAngularUpperLimit(btVector3(0,0,0));

      dynamicsWorld->addConstraint(pdl2XZplaneConstraint);
   }


}



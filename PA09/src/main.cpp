//MAIN DRIVER

//globals

//includes
#include "shader_loader.h"
#include "display.h"
#include "timer.h"
#include "keyboard.h"
#include "mouse.h"
#include "reshape.h"
#include "structure.h"

#include <GL/glut.h>
#include <bullet/btBulletDynamicsCommon.h>
#include <iostream>

//create object for variables
structure var;


//function prototypes
void init();


//main

int main( int argc, char **argv )
{

   //initialize glut
   var.window_w = 750;
   var.window_h = 750;
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
   glutInitWindowSize(var.window_w, var.window_h);
   glutCreateWindow("Air Hockey");

    GLenum status = glewInit();
    if( status != GLEW_OK)
    {
        std::cerr << "[F] GLEW NOT INITIALIZED: ";
        std::cerr << glewGetErrorString(status) << std::endl;
        return -1;
    }
   //run init
   init();
   
   initMenu();

   //glut callback functions
   glutDisplayFunc(display);
   glutTimerFunc(17, update, 0);
   glutKeyboardFunc(keyPress);
   glutSpecialFunc(special);
   glutSpecialUpFunc(specialUp);
   glutMouseFunc(mouseEvent);
   glutMotionFunc(mouseMove);
   glutReshapeFunc(reshape);

   glutMainLoop();
}


//function implementation
void init()
{
   //initalize openGL
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);

   //Load shaders 
   var.program = loadShaders(var.loc_position, var.loc_color, var.loc_mvpmat);

   //initialize board 
   var.board = new object("assets/models/board.obj", 0.5, var.program);

   // initialize boundaries for physics
   object walls("assets/models/walls.obj", 0.5, var.program);
 
   //initialize puck
   var.puck = new object("assets/models/puck.obj", 0.2, var.program);

   //initialize paddles
   var.paddle1 = new object("assets/models/paddle.obj", 0.2, var.program);
   var.paddle2 = new object("assets/models/paddle.obj", 0.2, var.program);

   var.board->initBuffers();
   walls.initBuffers();
   var.puck->initBuffers();
   var.paddle1->initBuffers();
   var.paddle2->initBuffers();

   // set initial position
   var.paddle1->adjust_translation( glm::vec3(-3.0, 0.0, 0.01));
   var.paddle2->adjust_translation( glm::vec3( 3.0, 0.0, 0.01));

   var.setPaddle1Place(glm::vec2(-3.0, 0.0)); 
   var.setPaddle2Place(glm::vec2(3.0, 0.0)); 


   //set attrib and uniform locations
   var.board->setAttribLocs(var.loc_position, var.loc_color, var.loc_mvpmat);
   walls.setAttribLocs(var.loc_position, var.loc_color, var.loc_mvpmat);
   var.puck->setAttribLocs(var.loc_position, var.loc_color, var.loc_mvpmat);
   var.paddle1->setAttribLocs(var.loc_position, var.loc_color, var.loc_mvpmat);
   var.paddle2->setAttribLocs(var.loc_position, var.loc_color, var.loc_mvpmat);

   //set camera position and perspective
   var.zNear = 0.01;
   var.zFar = 100.0;
   var.fov = 60.0;
   var.ratio = 1.0; 

   var.projection = glm::perspective(var.fov, var.ratio, var.zNear, var.zFar);

   // set up initial camera position
   var.camPos.x = 4.0;
   var.camPos.y = 3.0;
   var.camPos.z = 8.0;
   var.look.x = 0.0;
   var.look.y = 0.0;
   var.look.z = 0.0;
   var.up.x = 0.0;
   var.up.y = 1.0;
   var.up.z = 0.0;   

   var.view = glm::lookAt( var.camPos, var.look, var.up ); 

   glEnable(GL_DEPTH_TEST);
   glDepthFunc(GL_LESS);

   //get and set size of board
   glm::vec3 boardSize = var.board->get_dimensions();

   //get and set size of puck
   double radius = var.puck->x_width/2.0;
   double height = var.puck->y_width;
   glm::vec2 puckSize = glm::vec2( radius, height);

   //get and set size of paddles
   radius = var.paddle1->x_width/2.0;
   height = var.paddle1->y_width;
   glm::vec2 paddle1Size = glm::vec2(radius, height);

   radius = var.paddle2->x_width/2.0;
   height = var.paddle2->y_width;
   glm::vec2 paddle2Size = glm::vec2(radius, height);

   //get center of objects
   glm::vec3 boardCent = var.board->center;
   glm::vec3 puckCent = var.puck->center;
   glm::vec3 paddle1Cent = var.paddle1->center;
   glm::vec3 paddle2Cent = var.paddle2->center;

   //initialize physics

   var.phys.init( boardSize, puckSize, paddle1Size, paddle2Size,
                         boardCent, puckCent, paddle1Cent, paddle2Cent,
                         walls.geometry, walls.vCount);
}

#include "object.h"

object::object( const char* file, const double& sf, const GLuint prog )
{
   program = prog;
   scaleFactor = sf;
   filename = file;
   velocity = glm::vec3(0.0,0.0,0.0);
   translation.x = 0;
   translation.y = 0;
   translation.z = 0;

}

object::object()
{

}

object::~object()
{

}

bool object::loadOBJ(const char *file, Vertex* &geo )
{

    Assimp::Importer importer;

    //load file
    const aiScene *scene = importer.ReadFile(file, aiProcess_GenNormals |aiProcess_Triangulate );
	
    if(!scene)
    {
        printf("ERROR: scene failed to import.\n");
        return false;
    }

    //getting vertices
    aiVector3D *vs = (*scene->mMeshes)->mVertices;

    //getting number of vertices
    vCount = (*scene->mMeshes)->mNumVertices;

    //creating geometry of correct size
    geo = new Vertex[vCount];
	
    //adding v, uv, and n to the mesh
    for(int i = 0; i < vCount; i++)
    {
        // set vertices 
        geo[i].position[0] = scaleFactor*vs[i].x;
	geo[i].position[1] = scaleFactor*vs[i].y;
	geo[i].position[2] = scaleFactor*vs[i].z;
	
        // set hard-coded color 

        
	geo[i].color[0] = 0.0;
	geo[i].color[1] = 0.0;
	geo[i].color[2] = 0.0;	
    }
    return true;
}

void object::initBuffers()
{
   // load model
   loadOBJ( filename, geometry );

   //get width of object
   double x_min = geometry[0].position[0];
   double x_max = geometry[0].position[0];
   double y_min = geometry[0].position[1];
   double y_max = geometry[0].position[1];
   double z_min = geometry[0].position[2];
   double z_max = geometry[0].position[2];

   for( int i=0; i < vCount; i++){
      if( geometry[i].position[0] < x_min )
         x_min = geometry[i].position[0];
      if( geometry[i].position[0] > x_max )
         x_max = geometry[i].position[0];
      if( geometry[i].position[1] < y_min )
         y_min = geometry[i].position[1];
      if( geometry[i].position[1] > y_max )
         y_max = geometry[i].position[1];
      if( geometry[i].position[2] < z_min )
         z_min = geometry[i].position[2];
      if( geometry[i].position[2] > z_max )
         z_max = geometry[i].position[2];
   }
   x_width = x_max - x_min;
   y_width = y_max - y_min;
   z_width = z_max - z_min;
   center = glm::vec3( x_width/2.0+x_min, y_width/2.0+y_min, z_width/2.0+z_min);

   //initialize buffer object
   glGenBuffers(1, &vbo_geometry);
   glBindBuffer(GL_ARRAY_BUFFER, vbo_geometry);
   glBufferData(GL_ARRAY_BUFFER, vCount * sizeof(Vertex), geometry, GL_STATIC_DRAW);

}

void object::draw(glm::mat4& mvpMat, glm::mat4 viewMat, glm::mat4 projMat)
{
    // premultiply mvp
    mvpMat = projMat * viewMat * model;

    //upload the matrix to the shader
    glUniformMatrix4fv(loc_mvpmat, 1, GL_FALSE, glm::value_ptr(mvpMat));

    //set up the Vertex Buffer Object so it can be drawn
    glEnableVertexAttribArray(loc_position);
    glEnableVertexAttribArray(loc_color);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_geometry);
    //set pointers into the vbo for each of the attributes(position and color)
    glVertexAttribPointer( loc_position,//location of attribute
                           3,//number of elements
                           GL_FLOAT,//type
                           GL_FALSE,//normalized?
                           sizeof(Vertex),//stride
                           (void*)offsetof(Vertex,position));//offset

    glVertexAttribPointer( loc_color,
                           3,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof(Vertex),
                           (void*)offsetof(Vertex,color));

    glDrawArrays(GL_TRIANGLES, 0, vCount);//mode, starting index, count

    //clean up
    glDisableVertexAttribArray(loc_position);
    glDisableVertexAttribArray(loc_color);
}

void object::setAttribLocs(GLint locP, GLint locC, GLint locMVP)
{
   loc_position = locP;
   loc_color = locC;
   loc_mvpmat = locMVP;
}

void object::adjust_translation( const glm::vec3& motion )
{
   velocity = glm::vec3(motion.x,motion.y,motion.z);
   
   translation.x += motion.x;
   translation.y += motion.y;
   translation.z += motion.z;

   model = glm::translate( glm::mat4(1.0f), translation);
   glutPostRedisplay();
}

void object::set_translation( const glm::vec3& trans )
{
   velocity = glm::vec3(trans.x,trans.y,trans.z);
   
   translation.x = trans.x;
   translation.y = trans.y;
   translation.z = trans.z;

   model = glm::translate( glm::mat4(1.0f), translation);
   glutPostRedisplay();
}

void object::set_rotation( const glm::mat4& rotMat )
{
   rotation = rotMat;
}

glm::vec3 object::get_dimensions()const
{
   return glm::vec3(x_width, y_width, z_width);
}


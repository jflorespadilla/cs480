#include "reshape.h"

void reshape(int w, int h)
{
    var.window_w = w;
    var.window_h = h;
    //Change the viewport to be correct
    glViewport( 0, 0, w, h);

    //Update the projection matrix as well
    //See the init function for an explaination
    var.projection = glm::perspective(45.0f, float(var.window_w)/float(var.window_h), 0.01f, 100.0f);
}

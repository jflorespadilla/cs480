//structure implementation

#include "structure.h"


//constructor
structure::structure()
{
   p1_score = 0;
   p2_score = 0;
   pauseFlag = false;
   winnerFlag = 0;
}

//destructor
structure::~structure()
{

}

//set where paddles will be placed
void structure::setPaddle1Place(glm::vec2 place)
{

   //bounds of paddle
   place.x = max(-3.1188105f,place.x);
   place.x = min(-0.2f,place.x);

   // z
   place.y = min(1.749742f,place.y);
   place.y = max(-1.749742f,place.y);

   paddle1Place = place;

}

void structure::setPaddle2Place(glm::vec2 place)
{

   // simple bounding rectangle 
   place.x = min(3.1188105f,place.x);
   place.x = max(0.2f,place.x);

   // z
   place.y = min(1.749742f,place.y);
   place.y = max(-1.749742f,place.y);

   paddle2Place = place;

}


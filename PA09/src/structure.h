/*STRUCTURE CLASS- holds variables and parameters for program*/
#ifndef __STRUCTURE_H
#define __STRUCTURE_H

//includes
#include "object.h"
#include "physics.h"
#include "display.h"

#include <GL/glut.h>
#include <btBulletDynamicsCommon.h>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> //Makes passing matrices to shaders easier

class object;

class structure
{

   public:

      //constructor
      structure();


      //destructor
      ~structure();

      int x_beg, y_beg, x_cur, y_cur;

      GLuint program;

      glm::vec4 translation;
      glm::mat4 view;
      glm::mat4 rotation;
      glm::mat4 projection;
      glm::mat4 mvp;

      //attribute locations
      GLint loc_position;
      GLint loc_color;
      GLint loc_mvpmat;

      // for camera
      glm::vec3 camPos;
      glm::vec3 look;
      glm::vec3 up;
      GLfloat fov;
      GLfloat ratio;
      GLfloat zNear;
      GLfloat zFar;

      // paddle movement
      btScalar pdl1MaxVelocity;
      btScalar pdl2MaxVelocity;
      glm::vec2 paddle1Place;
      glm::vec2 paddle2Place;

      void setPaddle1Place(glm::vec2 place);
      void setPaddle2Place(glm::vec2 place);

      physics phys;

      int p1_score;
      int p2_score;
      int winnerFlag;

      //objects
      object* board;
      object* puck;
      object* paddle1; 
      object* paddle2; 

      int window_w;
      int window_h;

      bool pauseFlag;
};


#endif


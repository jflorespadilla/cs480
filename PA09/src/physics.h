#ifndef __PHYSICS_H
#define __PHYSICS_H
#include "object.h"
#include <vector>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <btBulletDynamicsCommon.h>

class physics
{
   public:
      physics();

      ~physics();

      void init( glm::vec3 const& boardSize, glm::vec2 const& puckSize, glm::vec2 const& paddle1Size, glm::vec2 const& paddle2Size,
                 glm::vec3 const& boardCent, glm::vec3 const& puckCent, glm::vec3 const& paddle1Cent, glm::vec3 const& paddle2Cent,
                 object::Vertex* geo, const int num_boardPoints);

      btCollisionShape* boardShape;
      btCollisionShape* puckShape;
      btCollisionShape* paddle1Shape;
      btCollisionShape* paddle2Shape;

      btCollisionShape* paddle1;
      btCollisionShape* paddle2;

      btRigidBody* puckRigidBody;
      btRigidBody* boardRigidBody;
      btRigidBody* paddle1RigidBody;
      btRigidBody* paddle2RigidBody;
      btDiscreteDynamicsWorld* dynamicsWorld;
     
      btGeneric6DofConstraint *puckXZplaneConstraint;
      btGeneric6DofConstraint *pdl1XZplaneConstraint;
      btGeneric6DofConstraint *pdl2XZplaneConstraint;

      btTransform puck_trans;
      btTransform paddle1_trans;
      btTransform paddle2_trans;
      
      glm::vec3 puck_pre_motion;
      glm::vec3 paddle1_pre_motion;
      glm::vec3 paddle2_pre_motion;
      glm::vec3 puck_post_motion;
      glm::vec3 paddle1_post_motion;
      glm::vec3 paddle2_post_motion;
      

      btDefaultCollisionConfiguration* collisionConfiguration;
      btCollisionDispatcher* dispatcher;
      btBroadphaseInterface* overlappingPairCache;
      btSequentialImpulseConstraintSolver* solver;

      btDefaultMotionState* boardMotionState;
      btDefaultMotionState* puckMotionState;
      btDefaultMotionState* paddle1MotionState;
      btDefaultMotionState* paddle2MotionState;

      btScalar board_friction;
      btScalar board_restitution;

      btScalar pdl_mass;
      btScalar pdl_friction;
      btScalar pdl_restitution;
      
      btScalar puck_mass;
      btScalar puck_friction;
      btScalar puck_restitution;

      int shift_z_pos1;
      int shift_z_neg1;
      int shift_z_pos2;
      int shift_z_neg2;
      int shift_x_pos1;
      int shift_x_neg1;
      int shift_x_pos2;
      int shift_x_neg2;

      int action;
};


#endif



#ifndef __OBJECT_H
#define __OBJECT_H

#include <string>
#include <iostream>
#include <vector>
#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> //Makes passing matrices to shaders easier
#include <ImageMagick/Magick++.h>
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "assimp/color4.h"

using namespace std;

class object
{
   public:

      object(const char* file, const double& scale, const GLuint prog);

      object();

      ~object();

      struct Vertex
      {
         GLfloat position[3];
         GLfloat normal[3];
         GLfloat color[3];
      };
      GLuint vbo_geometry;// VBO handle for our geometry
      Vertex* geometry;
      const char* filename;

      bool textureLoad();

      bool loadOBJ(const char *file, Vertex* &geo );

      void initBuffers();

      void draw(glm::mat4& mvpMat, glm::mat4 viewMat, glm::mat4 projMat);

      // attribute types
      GLuint program;

      //attribute locations
      GLint loc_position;
      GLint loc_color;
      GLint loc_mvpmat;
      void setAttribLocs(GLint locP, GLint locC, GLint locMVP); // used to get locations of attributes and uniforms from shader

      int vCount;// number of vertices
      glm::mat4 model;
      glm::vec3 translation;
      glm::mat4 rotation;
      glm::vec3 velocity;

      void adjust_translation( const glm::vec3& motion );
      void set_translation( const glm::vec3& trans );
      void set_rotation( const glm::mat4& rotMat );
      glm::vec3 get_dimensions()const;

      double x_width;
      double y_width;
      double z_width;
      glm::vec3 center;

      double scaleFactor;

};

#endif

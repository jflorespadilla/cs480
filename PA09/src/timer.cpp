#include "timer.h"



void update(int state)
{
   if(var.pauseFlag == false)
   {
   //step simulation
   var.phys.dynamicsWorld->stepSimulation(1.0f/60.f, 10);


   // update camera
   var.view = glm::lookAt( var.camPos, var.look, var.up ); 

   glm::vec3 motion;

   //get an update of the motion state
   var.phys.puckRigidBody->getMotionState()->getWorldTransform( var.phys.puck_trans );

      {
         var.phys.puckRigidBody->setAngularVelocity(btVector3(0.0,0.0,0.0));
         
         btMatrix3x3 rotation = var.phys.puck_trans.getBasis();

         glm::mat4 rotMat=glm::mat4(rotation[0][0],rotation[0][1],rotation[0][2],0.0,
                     rotation[1][0],rotation[1][1],rotation[1][2],0.0,
                     rotation[2][0],rotation[2][1],rotation[2][2],0.0,
                     0.0, 0.0, 0.0, 1.0);

         var.puck->set_rotation( rotMat );
      }

      //load new position for puck 
      motion = glm::vec3( var.phys.puck_trans.getOrigin().getX(), var.phys.puck_trans.getOrigin().getY(), var.phys.puck_trans.getOrigin().getZ());

      //push new position into puck
      var.puck->set_translation( motion );

      //load new position into structure for puck model
      motion = glm::vec3( var.phys.puck_trans.getOrigin().getX(), var.phys.puck_trans.getOrigin().getY(), var.phys.puck_trans.getOrigin().getZ());

      //push new position into puck
      var.puck->set_translation( motion );

      /* Paddle1 */

      //get an update of the motion state
      var.phys.paddle1RigidBody->getMotionState()->getWorldTransform( var.phys.paddle1_trans );
      
      {
         // set rotation to zero 
         var.phys.paddle1RigidBody->setAngularVelocity(btVector3(0.0,0.0,0.0));
         
         btMatrix3x3 rotation = var.phys.paddle1_trans.getBasis();

         glm::mat4 rotMat = glm::mat4(rotation[0][0],rotation[0][1],rotation[0][2],0.0,
                     rotation[1][0],rotation[1][1],rotation[1][2],0.0,
                     rotation[2][0],rotation[2][1],rotation[2][2],0.0,
                     0.0, 0.0, 0.0, 1.0);

         var.paddle1->set_rotation( rotMat );
      }


      //load new position into structure for paddle1 model
      motion = glm::vec3( var.phys.paddle1_trans.getOrigin().getX(), var.phys.paddle1_trans.getOrigin().getY(), var.phys.paddle1_trans.getOrigin().getZ());

      //push new position into paddle1
      var.paddle1->set_translation( motion );

      //move paddle 1
      {
         btScalar maxVel = var.pdl1MaxVelocity;

         // reduce velocity to zero
         var.phys.paddle1RigidBody->setLinearVelocity(btVector3(0.0,0.0,0.0));
         var.phys.paddle1RigidBody->setAngularVelocity(btVector3(0.0,0.0,0.0));

         //compute required force to move paddle to paddle1_dest in next step
         btVector3 pos_f, pos_i, dist;
         btScalar t = 1.f/60.f,
                  mass = var.phys.pdl_mass;
         pos_f = btVector3(var.paddle1Place[0], 0.0, var.paddle1Place[1]);
         pos_i = btVector3(motion.x, 0.0, motion.z);

         btScalar f_xVal = (pos_f.getX()-pos_i.getX())*mass/t/t;
         btScalar f_zVal = (pos_f.getZ()-pos_i.getZ())*mass/t/t;

         if ( maxVel > 0 )
         {
            // compute what the velocity will be
            btVector3 vel_new = btVector3(f_xVal * t / mass, 0.0, f_zVal * t / mass);
            btScalar vel_newMag = sqrt(vel_new.getX()*vel_new.getX()+vel_new.getZ()*vel_new.getZ());

            if ( vel_newMag > maxVel )
            {
               // scale down the force to limit the velocity
               f_xVal = f_xVal / vel_newMag * maxVel;
               f_zVal = f_zVal / vel_newMag * maxVel;
            }
         }

         // apply a force to the paddle
         btVector3 F = btVector3( f_xVal, 0.0, f_zVal );
         var.phys.paddle1RigidBody->applyCentralForce(F);
      }

      /* Paddle2 */
      //get an update of the motion state
      var.phys.paddle2RigidBody->getMotionState()->getWorldTransform( var.phys.paddle2_trans );
      
      {
         btMatrix3x3 rotation = var.phys.paddle2_trans.getBasis();

         glm::mat4 rotMat= glm::mat4(rotation[0][0],rotation[0][1],rotation[0][2],0.0,
                     rotation[1][0],rotation[1][1],rotation[1][2],0.0,
                     rotation[2][0],rotation[2][1],rotation[2][2],0.0,
                     0.0, 0.0, 0.0, 1.0);

         var.paddle2->set_rotation( rotMat );
      }


      //load new position into structure for paddle2 model
      motion = glm::vec3( var.phys.paddle2_trans.getOrigin().getX(), var.phys.paddle2_trans.getOrigin().getY(), var.phys.paddle2_trans.getOrigin().getZ());

      //push new position into paddle2
      var.paddle2->set_translation( motion );

      //move paddle 2
      {
         // reduce velocity to zero
         var.phys.paddle2RigidBody->setLinearVelocity(btVector3(0.0,0.0,0.0));
         var.phys.paddle2RigidBody->setAngularVelocity(btVector3(0.0,0.0,0.0));

         //compute required force to move paddle to paddle2_dest in next step
         btVector3 pos_f, pos_i, dist;
         btScalar t = 1.f/60.f,
                  mass = var.phys.pdl_mass;

         pos_f = btVector3(var.paddle2Place[0], 0.0, var.paddle2Place[1]);
         pos_i = btVector3(motion.x, 0.0, motion.z);

         btScalar f_xVal = (pos_f.getX()-pos_i.getX())*mass/t/t;
         btScalar f_zVal = (pos_f.getZ()-pos_i.getZ())*mass/t/t;

         if ( var.pdl2MaxVelocity > 0 )
         {
            // compute what the velocity will be
            btVector3 vel_new = btVector3(f_xVal * t / mass, 0.0, f_zVal * t / mass);
            btScalar vel_newMag = sqrt(vel_new.getX()*vel_new.getX()+vel_new.getZ()*vel_new.getZ());

            if ( vel_newMag > var.pdl2MaxVelocity )
            {
               // scale down the force to limit the velocity
               f_xVal = f_xVal / vel_newMag * var.pdl2MaxVelocity;
               f_zVal = f_zVal / vel_newMag * var.pdl2MaxVelocity;
            }
         }

         // apply a force to the paddle
         btVector3 F = btVector3( f_xVal, 0.0, f_zVal );
         var.phys.paddle2RigidBody->applyCentralForce(F);
      }

      // Check if the puck has scored 
      glm::vec3 puck_pos = glm::vec3( var.phys.puck_trans.getOrigin().getX(), var.phys.puck_trans.getOrigin().getY(), var.phys.puck_trans.getOrigin().getZ());

      bool p1scored = false, p2scored = false, offBoard = false;

      // first check if scoreing is possible

      if ( puck_pos.z > 2.2 || puck_pos.z < -2.2 ) { // offBoard
         offBoard = true;
      }
      else if ( puck_pos.z < 0.374856f && puck_pos.z > -0.374856f ) { // scoring possible
         if ( puck_pos.x > 3.6 )
            p1scored = true;
         else if ( puck_pos.x < -3.6 )
            p2scored = true;
      }
      else if ( puck_pos.x > 3.6 || puck_pos.x < -3.6 ) { // offBoard
         offBoard = true;
      }

      if ( p1scored || p2scored || offBoard )
      {
         //set puck for player 2
         if ( p2scored )
         {
            var.phys.puckRigidBody->setWorldTransform(btTransform(btQuaternion(1,0,0,0),btVector3(-1.2, 0, 0)));
            // add score
            var.p2_score++;

            if( var.p2_score > 3 )
            {
               var.p1_score = 0;
               var.p2_score = 0;
               var.winnerFlag = 2;
            }
         }

         //set puck for player 1 
         else if ( p1scored )
         {
            var.phys.puckRigidBody->setWorldTransform(btTransform(btQuaternion(1,0,0,0),btVector3(1.2, 0, 0)));
            var.p1_score++;
            
            if( var.p1_score > 3 )
            {
               var.p1_score = 0;
               var.p2_score = 0;
               var.winnerFlag = 1;
            }
         }

         //puck off board, reset
         else if ( offBoard )
         {
            var.phys.puckRigidBody->setWorldTransform(btTransform(btQuaternion(1,0,0,0),btVector3(0, 0, 0)));
         }

         var.phys.paddle1RigidBody->setWorldTransform(btTransform(btQuaternion(1,0,0,0),btVector3(-3, 0, 0)));
         var.phys.paddle2RigidBody->setWorldTransform(btTransform(btQuaternion(1,0,0,0),btVector3( 3, 0, 0)));

         var.setPaddle1Place(glm::vec2(-3.0, 0.0));
         var.setPaddle2Place(glm::vec2( 3.0, 0.0));

         //get an update of the motion state
         var.phys.puckRigidBody->getMotionState()->getWorldTransform( var.phys.puck_trans );

         //load new position for puck
         motion = glm::vec3( var.phys.puck_trans.getOrigin().getX(), var.phys.puck_trans.getOrigin().getY(), var.phys.puck_trans.getOrigin().getZ());

         //push new position
         var.puck->set_translation( motion );
  

         //get an update of the motion state
         var.phys.paddle1RigidBody->getMotionState()->getWorldTransform( var.phys.paddle1_trans );

         //load new position into structure for paddle1 model
         motion = glm::vec3( var.phys.paddle1_trans.getOrigin().getX(), var.phys.paddle1_trans.getOrigin().getY(), var.phys.paddle1_trans.getOrigin().getZ());

         //push new position into paddle1
         var.paddle1->set_translation( motion );

         //get an update of the motion state
         var.phys.paddle2RigidBody->getMotionState()->getWorldTransform( var.phys.paddle2_trans );

         //load new position into structure for paddle2 model
         motion = glm::vec3( var.phys.paddle2_trans.getOrigin().getX(), var.phys.paddle2_trans.getOrigin().getY(), var.phys.paddle2_trans.getOrigin().getZ());

         //push new position into paddle2
         var.paddle2->set_translation( motion );

         // reset velocities
         var.phys.paddle1RigidBody->setLinearVelocity( btVector3(0.0,0.0,0.0));
         var.phys.paddle1RigidBody->setAngularVelocity( btVector3(0.0,0.0,0.0));

         var.phys.paddle2RigidBody->setLinearVelocity( btVector3(0.0,0.0,0.0));
         var.phys.paddle2RigidBody->setAngularVelocity( btVector3(0.0,0.0,0.0));

         var.phys.puckRigidBody->setLinearVelocity( btVector3(0.0,0.0,0.0));
         var.phys.puckRigidBody->setAngularVelocity( btVector3(0.0,0.0,0.0));

      }
   

   glutPostRedisplay();
   }
   glutTimerFunc(17, update, 0);
}


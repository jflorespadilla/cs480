#include <iostream>
#include <fstream>
#include "shader_loader.h"

void readInput (const char* file, std::string& output)
{
    std::string buffer;
    std::ifstream fin;
    fin.open(file, std::ios::in);
    std::string stream = "";

    while(fin.good())
    {
        std::getline(fin, stream);
        buffer.append(stream); 
        buffer.append(1, '\n');
    }

    buffer.append(1, '\0');
    fin.close();
    output = buffer;
}


//load the shaders----Change to accomadate structure var
GLuint loadShaders(GLint& locP, GLint& locC, GLint& locMVP)
{
    GLenum err = glewInit();
    GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

    //Shader Sources
    std::string vsLoad;
    readInput("assets/shaders/shader.vert", vsLoad);
    const char *vs = vsLoad.c_str();
    std::string fsLoad;
    readInput("assets/shaders/shader.frag", fsLoad);
    const char *fs =fsLoad.c_str();


    //compile the shaders
    GLint shader_status;

    // Vertex shader first
    glShaderSource(vertex_shader, 1, &vs, NULL);
    glCompileShader(vertex_shader);
    //check the compile status
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &shader_status);
    if(!shader_status)
    {
        std::cerr << "[F] FAILED TO COMPILE VERTEX SHADER!" << std::endl;
        exit(EXIT_FAILURE);
    }

    // Now the Fragment shader
    glShaderSource(fragment_shader, 1, &fs, NULL);
    glCompileShader(fragment_shader);
    //check the compile status
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &shader_status);
    if(!shader_status)
    {
        std::cerr << "[F] FAILED TO COMPILE FRAGMENT SHADER!" << std::endl;
        exit(EXIT_FAILURE);
    }


    //Now we link the 2 shader objects into a program
    //This program is what is run on the GPU
    GLuint program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);
    //check if everything linked ok
    glGetProgramiv(program, GL_LINK_STATUS, &shader_status);
    if(!shader_status)
    {
        std::cerr << "[F] THE SHADER PROGRAM FAILED TO LINK" << std::endl;
        exit(EXIT_FAILURE);
    }

    //Now we set the locations of the attributes and uniforms
    locP = glGetAttribLocation(program,
                    const_cast<const char*>("v_position"));
    if(locP == -1)
    {
        std::cerr << "[F] POSITION NOT FOUND" << std::endl;
        exit(EXIT_FAILURE);
    }

    locC = glGetAttribLocation(program,
                    const_cast<const char*>("v_color"));
    if(locC == -1)
    {
        std::cerr << "[F] V_COLOR NOT FOUND" << std::endl;
        exit(EXIT_FAILURE);
    }

    locMVP = glGetUniformLocation(program,
                    const_cast<const char*>("mvpMatrix"));
    if(locMVP == -1)
    {
        std::cerr << "[F] MVPMATRIX NOT FOUND" << std::endl;
        exit(EXIT_FAILURE);
    }

    return program;
}

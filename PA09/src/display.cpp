#include "display.h"

void display()
{
   glClearColor( 0.0, 0.0, 0.2, 1.0 );
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

   displayScores();
 
   if(var.winnerFlag == 1 )
      displayWinner("PLAYER 1 WINS!");
 
   else if (var.winnerFlag == 2)
      displayWinner("PLAYER 2 WINS!");
 
   glUseProgram(var.program);

   var.board->draw(var.mvp, var.view, var.projection);      
   var.puck->draw(var.mvp, var.view, var.projection);
   var.paddle1->draw(var.mvp, var.view, var.projection);
   var.paddle2->draw(var.mvp, var.view, var.projection);

   glutSwapBuffers();
}
void initMenu()
{
   glutCreateMenu(menu);
   glutAddMenuEntry("Reset Game", 1);
   glutAddMenuEntry("Pause", 2);
   glutAddMenuEntry("Resume", 3);
   glutAddMenuEntry("Exit", 4);
   glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void menu(int key)
{
   switch(key)
   {
      case 1:
      {
         glm::vec3 motion;
         //reset score
         var.p1_score = 0;
         var.p2_score = 0;
         var.winnerFlag = 0;

         //reset puck and paddle position
         var.phys.puckRigidBody->setWorldTransform(btTransform(btQuaternion(1,0,0,0),btVector3(0, 0, 0)));
         var.phys.paddle1RigidBody->setWorldTransform(btTransform(btQuaternion(1,0,0,0),btVector3(-3, 0, 0)));
         var.phys.paddle2RigidBody->setWorldTransform(btTransform(btQuaternion(1,0,0,0),btVector3( 3, 0, 0)));

         var.setPaddle1Place(glm::vec2(-3.0, 0.0));
         var.setPaddle2Place(glm::vec2( 3.0, 0.0));

         //get an update of the motion state
         var.phys.puckRigidBody->getMotionState()->getWorldTransform( var.phys.puck_trans );

         //load new position into structure for paddle1 model
         motion = glm::vec3( var.phys.paddle1_trans.getOrigin().getX(), var.phys.paddle1_trans.getOrigin().getY(), var.phys.paddle1_trans.getOrigin().getZ());

         //push new position into paddle1
         var.puck->set_translation( motion );

         //get an update of the motion state
         var.phys.paddle1RigidBody->getMotionState()->getWorldTransform( var.phys.paddle1_trans );

         //load new position into structure for paddle1 model
         motion = glm::vec3( var.phys.paddle1_trans.getOrigin().getX(), var.phys.paddle1_trans.getOrigin().getY(), var.phys.paddle1_trans.getOrigin().getZ());

         //push new position into paddle1
         var.paddle1->set_translation( motion );

         //get an update of the motion state
         var.phys.paddle2RigidBody->getMotionState()->getWorldTransform( var.phys.paddle2_trans );

         //load new position into structure for paddle2 model
         motion = glm::vec3( var.phys.paddle2_trans.getOrigin().getX(), var.phys.paddle2_trans.getOrigin().getY(), var.phys.paddle2_trans.getOrigin().getZ());

         //push new position into paddle2
         var.paddle2->set_translation( motion );

         // reset velocities
         var.phys.paddle1RigidBody->setLinearVelocity( btVector3(0.0,0.0,0.0));
         var.phys.paddle1RigidBody->setAngularVelocity( btVector3(0.0,0.0,0.0));

         var.phys.paddle2RigidBody->setLinearVelocity( btVector3(0.0,0.0,0.0));
         var.phys.paddle2RigidBody->setAngularVelocity( btVector3(0.0,0.0,0.0));

         var.phys.puckRigidBody->setLinearVelocity( btVector3(0.0,0.0,0.0));
         var.phys.puckRigidBody->setAngularVelocity( btVector3(0.0,0.0,0.0));
         break;
      }
      case 2:
      var.pauseFlag = true;
      break;

      case 3: 
      var.pauseFlag = false;
      break;

      case 4:
      exit(0);
      break;
   }
}

void displayScores()
{
    // printing text
    const char* p1 = "Player 1:";
    const char* p2 = "Player 2:";
    const char* msg = "SCORE";

    // disable shaders for text
    glUseProgram(0);

    // setting color
    glColor3f(1.0f,1.0f,1.0f);

    // setting position
    glRasterPos2f(-0.95f,0.90f);
  
    // print to screen
    while (*p1) 
    {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *p1);
        p1++;
    }

    // setting color
    glColor3f(1.0f,1.0f,1.0f);

    // setting position
    glRasterPos2f(-0.18f,0.90f);

    while (*msg) 
    {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *msg);
        msg++;
    }

    // setting color
    glColor3f(1.0f,1.0f,1.0f);

    // setting position
    glRasterPos2f(0.6f,0.90f);

    while (*p2) 
    {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *p2);
        p2++;
    }
}

void displayWinner(const char* winner)
{
    // disable shaders for text
    glUseProgram(0);

    // setting color
    glColor3f(1.0f,1.0f,1.0f);

    // setting position
    glRasterPos2f(-0.95f,0.90f);
  
    // print to screen
    while (*winner) 
    {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *winner);
       winner++;
    }

}


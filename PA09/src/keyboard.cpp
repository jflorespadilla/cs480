#include "keyboard.h"

// for camera
void keyPress( unsigned char key, int x, int y )
{
   switch(key)
   {
      case 27:
      exit(0);
      break;

      case 'w':
      if(var.pauseFlag == false)
         var.camPos.z -= 1;
      break;

      case 's':
      if(var.pauseFlag == false)
         var.camPos.z += 1;
      break;

      case 'a':
      if(var.pauseFlag == false)
         var.camPos.x -= 1;
      break;

      case 'd':
      if(var.pauseFlag == false)
         var.camPos.x += 1;
      break;

      case 'q':
      if(var.pauseFlag == false)
         var.camPos.y -= 1;
      break;

      case 'e':
      if(var.pauseFlag == false)
         var.camPos.y += 1;
      break;

      case 'r':
      if(var.pauseFlag == false)
      {
         var.camPos.x = 4.0;
         var.camPos.y = 3.0;
         var.camPos.z = 8.0;
         var.look.x = 0.0;
         var.look.y = 0.0;
         var.look.z = 0.0;
      }
      break;

   }
}

//for paddle 1
void special( int key, int x, int y )
{
   switch( key )
   {
      case GLUT_KEY_UP:
          if(var.pauseFlag == false)
             var.paddle1Place[0] -= -0.05;
         break;

      case GLUT_KEY_DOWN:
          if(var.pauseFlag == false)
             var.paddle1Place[0] += -0.05;
         break;

      case GLUT_KEY_LEFT:
          if(var.pauseFlag == false)
             var.paddle1Place[1] += -0.05;
         break;

      case GLUT_KEY_RIGHT:
          if(var.pauseFlag == false)
             var.paddle1Place[1] -= -0.05;
         break;
   }
}
void specialUp( int key, int x, int y )
{

}


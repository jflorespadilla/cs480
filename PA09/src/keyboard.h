#ifndef __KEYBOARD_H
#define __KEYBOARD_H

#include <iostream>
#include <string>
#include "structure.h"
#include <btBulletDynamicsCommon.h>

extern structure var;

// for camera
void keyPress( unsigned char key, int x, int y );

//for paddle 1
void special( int key, int x, int y );
void specialUp( int key, int x, int y );

#endif

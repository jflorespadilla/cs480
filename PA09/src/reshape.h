#ifndef __RESHAPE_H
#define __RESHAPE_H

#include "structure.h"

extern structure var;

void reshape(int w, int h);

#endif

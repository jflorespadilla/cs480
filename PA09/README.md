Author: Jesus Flores-Padilla

Partners: Sarah Koh  Jan Chris Orofolf

Project: Air Hockey

Status: (We have something to turn in)

Note to the grader: Anything submitted right now is still subject to change. For the demo.

Instructsions:
Camera Controls: a, s, q, w, e, and r (Moves the camera, but in weird ways)

Paddle 1: Arrow Keys:- Up (move forward)
	   	 	Down (move back)
			Left(move left)
			Right(move right)
Paddle 2: -Mouse:- Left Click (allows for mouse control of the paddle. Mimics mouse movement.

The Run down of events and other such things:
So, before we go further into this, I'd like to recognize Sarah for her wonderful work. She's basically the reason our group has something to turn in. Were it not for her then this groupe would be doomed to oblivion.

Anyway. The structure has changed since PA8. There are more classes to work with and such. This made it easier to code and to keep things organized. This groupe ran with this architecture made by Sara solely for the fact that Sarah had something working, Chris and I didn't. Were it up to me, I'd have chosen a slightly different approach to the code architecture. I would probably build based on the generic game code set up, where everyting would have been defined as managers and we'd seperate functionality based on what they would relate to. (Things like stuff for camera control and display, physics, and game logic stuff such as points.)
But for what it's worth, this code is pretty good and close to what I had in mind. The only difference is that lack of pointers to a "main game loop."